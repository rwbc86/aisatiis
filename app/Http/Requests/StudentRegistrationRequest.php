<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;


class StudentRegistrationRequest extends FormRequest {

	public function rules()
	{
		return [
			'student.lastname' 		=> 'required',
			'student.firstname' 	=> 'required',
			'student.middlename' 	=> 'required',
			'student.address' 		=> 'required',
			'student.birth_date' 	=> 'required',
			'student.birth_place'	=> 'required',
			'student.gender'		=> 'required',
			'student.civil_status'	=> 'required',
			'student.citizenship' 	=> 'required',
			'student.religion' 		=> 'required',
			'student.email' 		=> 'required',
			'student.mobile' 		=> 'required',
			'student.is_employed' 	=> 'required',
			'student.mom_name' => 'required',
			'student.mom_mobile' => 'required',
			'student.dad_name' => 'required',
			'student.dad_mobile' => 'required',
			'student.mom_occupation' => 'required',
			'student.dad_occupation' => 'required',
			'student.parent_address' => 'required',
			'student.family_income' => 'required',
			'student.siblings' => 'required',
			'student.emergency_name' => 'required',
			'student.emergency_relationship' => 'required',
			'student.emergency_address' => 'required',
			'student.emergency_mobile' => 'required',
			'education.intermediate_school' => 'required',
			'education.intermediate_graduation' => 'required',
			'education.secondary_school' => 'required',
			'education.secondary_graduation' => 'required',
			'student.is_true' => 'required'
		];
	}

	public function authorize()
	{
		return true;	
	}
}