<?php namespace App\Http\Controllers;

use Auth;
use Request;
use App\Models\AccountModel;
use App\Models\EnrollModel;
use App\Models\StudentModel;
use App\Models\FeeModel;
use App\Models\EnrollFeesModel;

class AccountController extends Controller {

	public function select()
	{

		$data['accounts'] = EnrollModel::getAllWithBalance();
		$data['breadcrumbs'] = array();
		return view('account.select')->with($data);
	}

	public function view($id)
	{
		$data['account'] = EnrollModel::getEnrollInfo($id);
		$data['student'] = StudentModel::getStudentInfo($data['account']->student);
		$data['account_fees'] = EnrollFeesModel::getAll($id);
		$data['fees'] = FeeModel::getAllActive();

		$data['breadcrumbs'] = array();
		return view('account.view')->with($data);
	}

	public function ajaxAddFee()
	{
		$input = Request::input('fee');
		if(EnrollFeesModel::add($input)){
			return "success|Fee Added";
		}

	}

	public function ajaxListFees($id)
	{
		$data['account_fees'] = EnrollFeesModel::getAll($id);
		return view('account.ajax_list_fees')->with($data);
	}

}
