<?php namespace App\Http\Controllers;

use Request;
use App\Models\SubjectModel;
use App\Models\SubjectTypeModel;
use App\Models\ClassModel;

class SubjectController extends Controller {

	public function index()
	{
		$data['types'] = SubjectTypeModel::getAll();
		$data['subjects'] = SubjectModel::getAllWithPaginate(5);

		$data['title'] = "Subject";
		$data['subtitle'] = "List";
		$data['breadcrumbs'] = array('enrollment','subject');
		
		return view('admin.enrollment.subject')->with($data);
	}

	public function view($id)
	{

		$data['subject'] = SubjectModel::getSubjectInfo($id);
		$data['classes'] = ClassModel::getAllClassBySubjectWithPaginate(5,$id);

		$data['title'] = "Subject";
		$data['subtitle'] = "view";
		$data['breadcrumbs'] = array('enrollment','subject');
		
		return view('admin.enrollment.subject_view')->with($data);
	}

	public function save()
	{
		$input = Request::input();
		if(SubjectModel::save($input['subject']))
			return "success|Subject Added";
	}

	public function ajaxGetAll()
	{
		$data['subjects'] = SubjectModel::getAllWithPaginate(5);
		return view('admin.enrollment.ajaxgetallsubjects')->with($data);
	}

	public function confirmDelete($id)
	{
		$data['subject'] = SubjectModel::getSubjectInfo($id);

		$data['title'] = "Subject";
		$data['subtitle'] = "Delete";
		$data['breadcrumbs'] = array('enrollment','subject','delete');

		return view('admin.enrollment.subject_confirm_delete')->with($data);
	}

	public function delete($id)
	{
		if(SubjectModel::delete($id))
			return redirect()->route('subject');
	}

}
