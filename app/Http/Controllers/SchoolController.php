<?php namespace App\Http\Controllers;
use App\Models\SchoolModel;

class SchoolController extends Controller {

	public function ajaxGetAll()
	{
		$data['schools'] = SchoolModel::getAll();
		return view('school.ajaxgetall')->with($data);
	}

}
