<?php namespace App\Http\Controllers;

use Auth;
use Request;
use App\Models\EmployeeModel;

class EmployeeController extends Controller
{
	public function index()
	{
		$data['employees'] = EmployeeModel::getAllWithPaginate(1);
		$data['breadcrumbs'] = array();
		return view('admin.employee.index')->with($data);
	}

	public function add()
	{
		$data['employee_positions'] = EmployeeModel::getAllEmployeePositions();

		$data['breadcrumbs'] = array();
		return view('admin.employee.add')->with($data);
	}

	public function save()
	{
		$input = Request::input();

		if(EmployeeModel::save($input['employee']))
			return redirect()->route('employee');
	}

	public function delete($id)
	{
		if(EmployeeModel::delete($id))
			return redirect()->route('employee');
	}

	public function edit($id)
	{
		$data['employee'] = EmployeeModel::getInfo($id);
		$data['employee_positions'] = EmployeeModel::getAllEmployeePositions();

		$data['breadcrumbs'] = array();
		return view('admin.employee.edit')->with($data);
	}

	public function update()
	{
		$input = Request::input();

		if(EmployeeModel::update($input['employee']))
			return redirect()->route('employee');
	}
}