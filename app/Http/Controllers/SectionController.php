<?php namespace App\Http\Controllers;

use Request;
use App\Models\SectionModel;
use App\Models\ProgramModel;
use App\Models\SchoolYearModel;
use App\Models\SemesterModel;

class SectionController extends Controller {

	public function index()
	{
		$data['sections'] = SectionModel::getAllWithPaginate(5);
		$data['school_years'] = SchoolYearModel::getAll();
		$data['semesters'] = SemesterModel::getAll();
		$data['programs'] = ProgramModel::getAll();

		$data['title'] = "Sections";
		$data['subtitle'] = "List";
		$data['breadcrumbs'] = array('section');
		
		return view('admin.enrollment.section')->with($data);
	}

	public function save()
	{
		$input = Request::input('section');
		if(SectionModel::save($input))
			return "success|Section Added";
	}

	public function ajaxGetAll()
	{
		$data['sections'] = SectionModel::getAllWithPaginate();
		return view('admin.enrollment.ajaxgetallsections')->with($data);
	}
}
