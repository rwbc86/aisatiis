<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Request;
use Storage;
use Illuminate\Routing\Controller;
use App\Http\Requests\StudentRegistrationRequest;

// Load Models
use App\Models\CitizenshipModel;
use App\Models\CivilStatusModel;
use App\Models\ProgramModel;
use App\Models\ReligionModel;
use App\Models\SchoolModel;
use App\Models\SchoolYearModel;
use App\Models\SemesterModel;
use App\Models\StudentModel;
use App\Models\SectionModel;
use App\Models\ClassModel;
use App\Models\EnrollModel;
use App\Models\FeeModel;


class StudentController extends Controller {

	public function ajaxEnrollClassAdd()
	{
		$input = Request::input('classes');
		if(ClassModel::enrollClass($input)){
			FeeModel::updateTuitionFee($input['enroll_id']);
			return "success|Class Enrolled";
		}
	}

	public function ajaxEnrollClassDelete()
	{
		$input = Request::input('class_id');
		$enroll_id = Request::input('enroll_id');
		if(ClassModel::unenrollClass($input))
			FeeModel::updateTuitionFee($enroll_id);
			return "success|Class Removed";
	}

	public function ajaxGetAllEnrolledClasses($enroll_id)
	{
		$data['enrolled_classes'] = ClassModel::getEnrolledClasses($enroll_id);
		$data['total_units'] = 0;

		foreach($data['enrolled_classes'] as $class){
			$data['total_units'] += $class->units;
		}

		return view('student.ajax_enrolled_subjects')->with($data);
	}

	public function index()
	{
		$data['students_with_pagination'] = StudentModel::getAllWithPaginate();
		$data['students'] = StudentModel::getAll();
		$data['title'] = "Students";
		$data['subtitle'] = "All Students";
		$data['breadcrumbs'] = array('students');
		
		
		return view('student.index')->with($data);
	}

	public function edit($id)
	{
		$data['student'] = StudentModel::getStudentInfo($id);
		$data['student_educ'] = StudentModel::getStudentEducationInfo($id);
		$data['enrolls'] = EnrollModel::getAllEnrollsByStudentId($id);
		$data['students'] = StudentModel::getAll();

		$data['citizenships'] = CitizenshipModel::getAll();
		$data['religions'] = ReligionModel::getAll();
		$data['schools'] = SchoolModel::getAll();


		$data['breadcrumbs'] = array('students');
		
		return view('student.edit')->with($data);
	}

	public function enroll($student_id)
	{
		$data['student'] = StudentModel::getStudentInfo($student_id);
		$data['semesters'] = SemesterModel::getAll();
		$data['programs'] = ProgramModel::getAll();
		$data['school_years'] = SchoolYearModel::getAll();
		$data['sections'] = SectionModel::getAllActive();

		$data['breadcrumbs'] = array('students');
		
		return view('student.enroll')->with($data);
	}

	public function enrollSubjects($enroll_id)
	{
		$data['enroll_session'] = EnrollModel::getEnrollSession($enroll_id);
		$data['classes'] = ClassModel::getAllActive();
		$data['enrolled_classes'] = ClassModel::getEnrolledClasses($enroll_id);
		$data['student'] = StudentModel::getStudentInfo($data['enroll_session']->student);
		$data['total_units'] = 0;

		$data['breadcrumbs'] = array('students');

		foreach($data['enrolled_classes'] as $class){
			$data['total_units'] += $class->units;
		}

		return view('student.enroll_subjects')->with($data);
	}

	public function enrollSessionSave()
	{
		$input = Request::input('enroll');
		
		$enroll_id = EnrollModel::saveEnrollSession($input);

		// AUTO LOAD CLASSES BY SECTION
		$enroll_session = EnrollModel::getEnrollSession($enroll_id);
		$classes = ClassModel::getClassesBySection($enroll_session->section);
		foreach($classes as $class){
			ClassModel::enrollClass([ 'enroll_id' => $enroll_id, 'student_id' => $enroll_session->student, 'class_id' => $class->id]);
		}
		// END AUTO LOAD

		return redirect()->route('student.enroll.subjects', $enroll_id);
	}

	public function showProfile($id)
	{
		$data['student'] = StudentModel::getStudentInfo($id);
		$data['student_educ'] = StudentModel::getStudentEducationInfo($id);

		$data['student']->civil_status = CivilStatusModel::getNameById($data['student']->civil_status);
		$data['student']->citizenship = CitizenshipModel::getNameById($data['student']->citizenship);
		$data['student']->religion = ReligionModel::getNameById($data['student']->religion);
		$data['enrolls'] = EnrollModel::getAllEnrollsByStudentId($id);

		$data['breadcrumbs'] = array('students');
		
		return view('student.profile')->with($data);
	}

	public function editPhoto($id)
	{
		$data['student'] = StudentModel::getStudentNameAndPhoto($id);
		$data['id'] = $id;

		$data['title'] = "Profile";
		$data['subtitle'] = "Change Photo";
		$data['breadcrumbs'] = array('students');
		
		return view('student.changephoto')->with($data);
	}

	public function savePhoto()
	{
		$input = Request::input();
		if(StudentModel::savePhoto($input))
			return redirect()->route('student.profile', $input['id']);
	}

	public function register()
	{
		$data['citizenships'] = CitizenshipModel::getAll();
		$data['religions'] = ReligionModel::getAll();
		$data['schools'] = SchoolModel::getAll();

		return view('student.register')->with($data);
	}

	public function registerSuccess()
	{
		return view('student.registersuccess');
	}

	public function update()
	{
		$student = Request::input('student');
		$education = Request::input('education');
		$id = Request::input('student_id');

		@$student['birth_date'] = date_format( date_create_from_format('m/d/Y', $student['birth_date']), 'Y-m-d' );
		$student['last_modified'] = date("Y-m-d h:i:s");

		DB::table('students')
			->where('id', $id)
			->update($student);

		$primary = DB::table('students_education')->where('student', $id)->where('education_level', 1)->get();

		if(count($primary) == 0)
		{
			DB::table('students_education')->insert(['student' => $id, 'education_level' => 1, 'school' => $education['primary_school'],
				'section' => $education['primary_section'],	'graduation' => $education['primary_graduation']]);
		}
		if(count($primary) != 0 AND $education['primary_school'] != 0)
		{
			DB::table('students_education')->where('id',$primary[0]->id)->update(['student' => $id, 'education_level' => 1, 'school' => $education['primary_school'],
				'section' => $education['primary_section'],	'graduation' => $education['primary_graduation']]);
		}

		$intermediate = DB::table('students_education')->where('student', $id)->where('education_level', 2)->get();

		if(count($intermediate) == 0)
		{
			DB::table('students_education')->insert(['student' => $id, 'education_level' => 2, 'school' => $education['intermediate_school'],
				'section' => $education['intermediate_section'],	'graduation' => $education['intermediate_graduation']]);
		}
		if(count($intermediate) != 0 AND $education['intermediate_school'] != 0)
		{
			DB::table('students_education')->where('id',$intermediate[0]->id)->update(['student' => $id, 'education_level' => 2, 'school' => $education['intermediate_school'],
				'section' => $education['intermediate_section'],	'graduation' => $education['intermediate_graduation']]);
		}

		$secondary = DB::table('students_education')->where('student', $id)->where('education_level', 3)->get();

		if(count($secondary) == 0)
		{
			DB::table('students_education')->insert(['student' => $id, 'education_level' => 3, 'school' => $education['secondary_school'],
				'section' => $education['secondary_section'],	'graduation' => $education['secondary_graduation']]);
		}
		if(count($secondary) != 0 AND $education['secondary_school'] != 0)
		{
			DB::table('students_education')->where('id',$secondary[0]->id)->update(['student' => $id, 'education_level' => 3, 'school' => $education['secondary_school'],
				'section' => $education['secondary_section'],	'graduation' => $education['secondary_graduation']]);
		}

		$tertiary = DB::table('students_education')->where('student', $id)->where('education_level', 4)->get();

		if(count($tertiary) == 0)
		{
			DB::table('students_education')->insert(['student' => $id, 'education_level' => 4, 'school' => $education['tertiary_school'],
				'section' => $education['tertiary_section'],	'graduation' => $education['tertiary_graduation']]);
		}
		if(count($tertiary) != 0 AND $education['tertiary_school'] != 0)
		{
			DB::table('students_education')->where('id',$tertiary[0]->id)->update(['student' => $id, 'education_level' => 4, 'school' => $education['tertiary_school'],
				'section' => $education['tertiary_section'],	'graduation' => $education['tertiary_graduation']]);
		}

		$vocational = DB::table('students_education')->where('student', $id)->where('education_level', 5)->get();

		if(count($vocational) == 0)
		{
			DB::table('students_education')->insert(['student' => $id, 'education_level' => 5, 'school' => $education['vocational_school'],
				'section' => $education['vocational_section'],	'graduation' => $education['vocational_graduation']]);
		}
		if(count($vocational) != 0 AND $education['vocational_school'] != 0)
		{
			DB::table('students_education')->where('id',$vocational[0]->id)->update(['student' => $id, 'education_level' => 5, 'school' => $education['vocational_school'],
				'section' => $education['vocational_section'],	'graduation' => $education['vocational_graduation']]);
		}

		return redirect()->route('student.profile', $id);
	}

	public function save(StudentRegistrationRequest $request)
	{

		$student = Request::input('student');
		$photo = Request::input('photo');
		$education = Request::input('education');
		$student['birth_date'] = date_format( date_create_from_format('m/d/Y', $student['birth_date']), 'Y-m-d' );
		$student['date_added'] = date("Y-m-d h:i:s");

		$id = DB::table('students')->insertGetId($student);
		$id_number = date('y')."-".$id;

		if ( "" != $photo )
		{
			$pic_bin = base64_decode($photo);
			$filename = strtolower($student['lastname'].$student['firstname']."_".$id.".jpg");
			$filename = str_replace('ñ', '', $filename);
			$filename = str_replace('Ñ', '', $filename);
			$filename = str_replace(' ', '', $filename);
			$path = generateUploadDir($filename);

			$disk = Storage::disk('students');
			if (!$disk->exists($path.$filename))
				$disk->makeDirectory($path);

			$disk->put($path.$filename, $pic_bin);

			DB::table('students')
					->where('id', $id)
					->update(['photo' => $filename]);
		}

		DB::table('students')
					->where('id', $id)
					->update(['id_number' => $id_number]);

		DB::table('students_education')
				->insert([
						'student' => $id,
						'education_level' => 1,
						'school' => $education['primary_school'],
						'section' => $education['primary_section'],
						'graduation' => $education['primary_graduation']
					]);

		DB::table('students_education')
				->insert([
						'student' => $id,
						'education_level' => 2,
						'school' => $education['intermediate_school'],
						'section' => $education['intermediate_section'],
						'graduation' => $education['intermediate_graduation'],
						'honors' => $education['intermediate_honor'],
						'awards' => $education['intermediate_awards']
					]);


		DB::table('students_education')
				->insert([
						'student' => $id,
						'education_level' => 3,
						'school' => $education['secondary_school'],
						'section' => $education['secondary_section'],
						'graduation' => $education['secondary_graduation'],
						'honors' => $education['secondary_honor'],
						'awards' => $education['secondary_awards']
					]);

		DB::table('students_education')
				->insert([
						'student' => $id,
						'education_level' => 4,
						'school' => $education['tertiary_school'],
						'section' => $education['tertiary_section'],
						'graduation' => $education['tertiary_graduation']
					]);

		DB::table('students_education')
				->insert([
						'student' => $id,
						'education_level' => 5,
						'school' => $education['vocational_school'],
						'section' => $education['vocational_section'],
						'graduation' => $education['vocational_graduation']
					]);

		return redirect()->route('student.register.success');
	}

}
