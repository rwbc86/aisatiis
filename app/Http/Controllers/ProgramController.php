<?php namespace App\Http\Controllers;

use Request;
use App\Models\ProgramModel;
use App\Models\SubjectModel;
use App\Models\ProgramDegreeModel;

class ProgramController extends Controller {

	public function addSubject($program_id)
	{
		$data['program'] = ProgramModel::getProgramById($program_id);
		$data['subjects'] = SubjectModel::getAll();

		$data['title'] = "Program";
		$data['subtitle'] = "Add Subject";
		$data['breadcrumbs'] = array('enrollment','program');
		
		return view('admin.enrollment.program_subject_add')->with($data);
	}

	public function ajaxGetAll()
	{
		$data['programs'] = ProgramModel::getAllWithPaginate(5);
		return view('admin.enrollment.ajaxgetallprograms')->with($data);
	}

	public function edit($id)
	{
		$data['program'] = ProgramModel::getProgramById($id);

		$data['title'] = "Program";
		$data['subtitle'] = "Edit";
		$data['breadcrumbs'] = array('enrollment','program');
		
		return view('admin.enrollment.program_edit')->with($data);
	}

	public function index()
	{
		$data['degrees'] = ProgramDegreeModel::getAll();
		$data['programs'] = ProgramModel::getAllWithPaginate(5);

		$data['title'] = "Program";
		$data['subtitle'] = "List";
		$data['breadcrumbs'] = array('enrollment','program');
		
		return view('admin.enrollment.program')->with($data);
	}

	public function save()
	{
		$input = Request::input();
		if(ProgramModel::save($input['program']))
			return "success|Program Added";
	}

	public function saveSubject()
	{
		$input = Request::input('program_subject');
		if(ProgramModel::saveSubject($input))
			return redirect()->route('program.edit',$input['program']);
	}

}
