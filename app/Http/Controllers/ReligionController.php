<?php namespace App\Http\Controllers;
use App\Models\ReligionModel;

class ReligionController extends Controller {

	public function ajaxGetAll()
	{
		$data['religions'] = ReligionModel::getAll();
		return view('religion.ajaxgetall')->with($data);
	}

}
