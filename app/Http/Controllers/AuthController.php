<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController extends Controller {

	public function login(Request $request)
	{

		if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')]))
            return redirect()->route('dashboard');
        else
        	return redirect()->back()->with('alert', ['type' => 'danger', 'message' => 'Invalid Login']);
	}

	public function logout()
	{
		Auth::logout();
		return redirect()->route('index');
	}

}
