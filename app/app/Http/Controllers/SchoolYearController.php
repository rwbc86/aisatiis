<?php namespace App\Http\Controllers;

use Request;
use App\Models\SchoolYearModel;

class SchoolYearController extends Controller {

	public function index()
	{
		$data['school_years'] = SchoolYearModel::getAllWithPaginate(5);

		$data['title'] = "School Years";
		$data['subtitle'] = "List";
		$data['breadcrumbs'] = array('school year');
		
		return view('admin.enrollment.schoolyear')->with($data);
	}

	public function save()
	{
		$input = Request::input();
		if(SchoolYearModel::save($input))
			return "success|School Year Added";
	}

	public function ajaxGetAll()
	{
		$data['school_years'] = SchoolYearModel::getAllWithPaginate();
		return view('admin.enrollment.ajaxgetallschoolyears')->with($data);
	}

}
