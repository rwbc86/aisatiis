<?php namespace App\Http\Controllers;

use Request;

class FeeController extends Controller {

	public function addCategory()
	{

		$data['title'] = "Fee";
		$data['subtitle'] = "add Category";
		$data['breadcrumbs'] = array('admin','fee');

		return view('admin.accounting.fee.add_category')->with($data);
	}

}