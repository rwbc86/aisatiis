<?php namespace App\Http\Controllers;

use Request;
use App\Models\ClassModel;
use App\Models\SubjectModel;
use App\Models\SchoolYearModel;
use App\Models\SemesterModel;
use App\Models\SectionModel;
use App\Models\EmployeeModel;

class ClassController extends Controller {

	public function add($subject_id)
	{

		$data['subject'] = SubjectModel::getSubjectInfo($subject_id);
		$data['school_years'] = SchoolYearModel::getAll('desc');
		$data['semesters'] = SemesterModel::getAll();
		$data['sections'] = SectionModel::getAllActive();
		$data['faculties'] = EmployeeModel::getAllFaculty();

		$data['title'] = "Class";
		$data['subtitle'] = "add";
		$data['breadcrumbs'] = array('enrollment','subject');
		
		return view('admin.enrollment.class_add')->with($data);
		
	}

	public function save()
	{
		$input = Request::input('class');
		if(ClassModel::save($input))
			return redirect()->route('subject.view',$input['subject_id']);
	}
}