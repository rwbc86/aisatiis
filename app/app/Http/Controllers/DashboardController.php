<?php namespace App\Http\Controllers;

use Session;
use Auth;

class DashboardController extends Controller {

	public function index()
	{
		$data['breadcrumbs'] = array();
		return view('dashboard')->with($data);
	}

}
