<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// AUTH ROUTES
Route::get('/', [ 'as' => 'index', 'uses' => 'IndexController@index' ]);
Route::post('login', [ 'as' => 'login', 'uses' => 'AuthController@login' ]);

// STUDENT ROUTES
Route::get('student/register', [ 'as' => 'student.register', 'uses' => 'StudentController@register' ]);
Route::get('student/register/success', [ 'as' => 'student.register.success', 'uses' => 'StudentController@registerSuccess' ]);
Route::post('student/save', [ 'as' => 'student.save', 'uses' => 'StudentController@save' ]);

// SCHOOL ROUTES
Route::get('school/ajax/all', [ 'as' => 'school.ajax.all', 'uses' => 'SchoolController@ajaxGetAll' ]);

Route::get('photos/{filename}', function($filename){
	$path = generateUploadDir($filename);
	$file = storage_path('app/students/') . $path . $filename;
	return response()->download($file);
});


// LOGIN REQUIRED
Route::group(['middleware' => 'auth'], function(){

	// AUTH ROUTES
	Route::get('logout', [ 'as' => 'logout', 'uses' => 'AuthController@logout' ]);

	// DASHBOARD ROUTES
	Route::get('dashboard', [ 'as' => 'dashboard', 'uses' => 'DashboardController@index' ]);

	// ACCOUNTING ROUTES
	Route::get('transaction/new', ['as' => 'transaction.select', 'uses' => 'TransactionController@select']);

	Route::get('account/select', ['as' => 'account.select', 'uses' => 'AccountController@select']);
	Route::get('account/view/{id}', ['as' => 'account.view', 'uses' => 'AccountController@view']);
	Route::post('account/ajax/add/fee/', ['as' => 'account.ajax.add.fee', 'uses' => 'AccountController@ajaxAddFee']);
	Route::get('account/{id}/ajax/list/fees', ['as' => 'account.ajax.list.fees', 'uses' => 'AccountController@ajaxListFees']);


	// STUDENT ROUTES
	Route::get('student/', [ 'as' => 'student', 'uses' => 'StudentController@index' ]);
	
	Route::get('student/profile/{id}', [ 'as' => 'student.profile', 'uses' => 'StudentController@showProfile' ]);
	Route::get('student/profile/photo/edit/{id}', [ 'as' => 'student.profile.photo.edit', 'uses' => 'StudentController@editPhoto' ]);
	Route::post('student/profile/photo/save', [ 'as' => 'student.profile.photo.save', 'uses' => 'StudentController@savePhoto' ]);

	Route::get('student/{id}/edit', [ 'as' => 'student.edit', 'uses' => 'StudentController@edit' ]);
	Route::post('student/update', [ 'as' => 'student.update', 'uses' => 'StudentController@update' ]);

	Route::get('student/enroll/{id}', [ 'as' => 'student.enroll', 'uses' => 'StudentController@enroll' ]);
	Route::post('student/enroll/session/save/', [ 'as' => 'student.enroll.session.save', 'uses' => 'StudentController@enrollSessionSave' ]);
	Route::get('student/enroll/subjects/{enroll_id}/', [ 'as' => 'student.enroll.subjects', 'uses' => 'StudentController@enrollSubjects' ]);
	Route::get('student/enroll/{enroll_id}/class/ajax/all/', [ 'as' => 'student.enroll.classes.ajax.all', 'uses' => 'StudentController@ajaxGetAllEnrolledClasses' ]);
	Route::post('student/enroll/class/ajax/add', [ 'as' => 'student.enroll.class.ajax.add', 'uses' => 'StudentController@ajaxEnrollClassAdd' ]);
	Route::post('student/enroll/class/ajax/delete', [ 'as' => 'student.enroll.class.ajax.delete', 'uses' => 'StudentController@ajaxEnrollClassDelete' ]);

	//ADMIN ROUTES
	Route::get('admin/school-year', ['as' => 'school_year', 'uses' => 'SchoolYearController@index']);
	Route::get('admin/school-year/ajax/all', ['as' => 'school_year.ajax.all', 'uses' => 'SchoolYearController@ajaxGetAll']);
	Route::post('admin/school-year/save', ['as' => 'school_year.save', 'uses' => 'SchoolYearController@save']);

	Route::get('admin/section', ['as' => 'section', 'uses' => 'SectionController@index']);
	Route::get('admin/section/ajax/all', ['as' => 'section.ajax.all', 'uses' => 'SectionController@ajaxGetAll']);
	Route::post('admin/section/save', ['as' => 'section.save', 'uses' => 'SectionController@save']);

	Route::get('admin/subject', ['as' => 'subject', 'uses' => 'SubjectController@index']);
	Route::get('admin/subject/{id}/view', ['as' => 'subject.view', 'uses' => 'SubjectController@view']);
	Route::get('admin/subject/ajax/all', ['as' => 'subject.ajax.all', 'uses' => 'SubjectController@ajaxGetAll']);
	Route::post('admin/subject/save', ['as' => 'subject.save', 'uses' => 'SubjectController@save']);

	Route::get('admin/class/{subject_id}/add/', ['as' => "class.add", 'uses' => 'ClassController@add']);
	Route::post('admin/class/save', ['as' => 'class.save', 'uses' => 'ClassController@save']);

	Route::get('admin/program', ['as' => 'program', 'uses' => 'ProgramController@index']);
	Route::get('admin/program/ajax/all', ['as' => 'program.ajax.all', 'uses' => 'ProgramController@ajaxGetAll']);
	Route::get('admin/program/{id}/edit', ['as' => 'program.edit', 'uses' => 'ProgramController@edit']);
	Route::get('admin/program/subject/{subject_id}/add', ['as' => 'program.subject.add', 'uses' => 'ProgramController@addSubject']);
	Route::post('admin/program/subject/save', ['as' => 'program.subject.save', 'uses' => 'ProgramController@saveSubject']);
	Route::post('admin/program/save', ['as' => 'program.save', 'uses' => 'ProgramController@save']);

	Route::get('admin/fee/category/add', ['as' => 'fee.category.add', 'uses' => 'FeeController@addCategory']);
	
});