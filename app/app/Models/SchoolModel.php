<?php namespace App\Models;

use DB;

class SchoolModel {

	public static function getAll()
	{
		return DB::table('school')->get();
	}

}