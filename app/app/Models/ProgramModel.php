<?php namespace App\Models;

use DB;


class ProgramModel {

	const TABLE = "program";

	public static function getAll()
	{
		return DB::table(self::TABLE)->get();
	}

	public static function getAllWithPaginate($items)
	{
		return DB::table(self::TABLE)->orderBy('id')->paginate($items);
	}

	public static function getProgramById($id)
	{
		return DB::table(self::TABLE)->where('id',$id)->first();
	}

	public static function save($input)
	{
		DB::table(self::TABLE)->insert($input);
		return true;
	}

	public static function saveSubject($input)
	{
		DB::table('program_subjects')->insert($input);
		return true;
	}


}