<?php namespace App\Models;

use DB;


class ClassModel {

	public static function getAllActive()
	{
		return DB::table('class AS a')
		->select('a.id', 'b.code', 'b.name', 'c.code AS section')
		->join('subjects AS b', 'b.id', '=', 'a.subject_id')
		->join('section AS c', 'c.id', '=', 'a.section_id')
		->get();
	}

	public static function enrollClass($input)
	{
		return DB::table('enroll_class')->insert($input);
	}

	public static function unEnrollClass($input)
	{
		return DB::table('enroll_class')->where('id', $input)->delete();
	}

	public static function getAllClassBySubjectWithPaginate($items,$subject_id)
	{
		return DB::table('class')
			->select('class.id', 'school_year.school_year', 'semester.name', 'section.code', 'employee.first_name', 'employee.last_name', 'class.is_closed', 'class.capacity')
			->join('school_year','class.sy_id','=','school_year.id')
			->join('semester','class.semester_id','=','semester.id')
			->join('section','class.section_id','=','section.id')
			->join('employee','class.instructor_id','=','employee.id')
			->where('subject_id',$subject_id)
			->orderBy('class.id','desc')->paginate($items);
	}

	public static function getClassesBySection($id)
	{
		return DB::table('class')
			->select('subjects.code', 'subjects.name', 'subjects.units', 'class.instructor_id', 'class.id')
			->join('subjects','subjects.id','=','class.subject_id')
			->where('class.section_id', $id)
			->get();
	}

	public static function getEnrolledClasses($enroll_id)
	{
		return DB::table('enroll_class AS a')
			->select('a.id', 'c.code', 'c.name', 'c.units', 'd.first_name', 'd.last_name' )
			->join('class AS b','b.id','=','a.class_id')
			->join('subjects AS c', 'c.id', '=', 'b.subject_id')
			->join('employee AS d', 'd.id', '=', 'b.instructor_id')
			->where('a.enroll_id', $enroll_id)
			->get();
	}

	public static function save($input)
	{
		DB::table('class')->insert($input);
		return true;
	}


}