<?php namespace App\Models;

use DB;


class SemesterModel {

	public static function getAll($order = "asc")
	{
		return DB::table('semester')->orderBy('id',$order)->get();
	}

}