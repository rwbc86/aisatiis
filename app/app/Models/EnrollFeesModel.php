<?php namespace App\Models;

use DB;


class EnrollFeesModel {

	public static function add($input)
	{
		$input['amount_due'] = DB::table('fees')->where('id',$input['fee_id'])->pluck('amount');

		return DB::table('enroll_fees')->insert($input);
	}

	public static function getAll($id)
	{
		return DB::table('enroll_fees AS a')
		->select('b.name', 'a.amount_due')
		->join('fees as b', 'b.id', '=', 'a.fee_id')
		->where('a.enroll_id', $id)
		->get();
	}
}