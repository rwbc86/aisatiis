<?php namespace App\Models;

use DB;


class EnrollModel {

	public static function saveEnrollSession($input)
	{
		return DB::table('enroll')->insertGetId($input);
	}

	public static function getAllEnrollsByStudentId($id)
	{
		return DB::table('enroll')
			->select('enroll.id', 'enroll.is_enrolled', 'program.name AS program', 'semester.name AS semester', 'school_year.school_year' )
			->join('program','program.id','=','enroll.program')
			->join('semester','semester.id','=','enroll.semester')
			->join('school_year','school_year.id','=','enroll.sy')
			->where('enroll.student', $id)
			->orderBy('enroll.id', 'desc')
			->get();
	}

	public static function getEnrollSession($id)
	{
		return DB::table('enroll')->where('id', $id)->first();
	}

	public static function getAllActive()
	{
		return DB::table('enroll AS a')
		->select('a.id', 'b.lastname', 'b.firstname', 'c.name AS program', 'd.name AS semester', 'e.school_year')
		->join('students AS b', 'b.id', '=', 'a.student')
		->join('program AS c','c.id','=','a.program')
		->join('semester AS d','d.id','=','a.semester')
		->join('school_year AS e','e.id','=','a.sy')
		->get();
	}

	public static function getAllWithBalance()
	{
		return DB::table('enroll AS a')
		->select('a.id', 'a.student', 'a.amount_due', 'a.balance', 'a.date_enrolled', 'b.id_number', 'b.firstname', 'b.lastname')
		->join('students AS b', 'b.id', '=', 'a.student')
		->where('a.balance', '>', 0)
		->get();
	}

	public static function getEnrollInfo($id)
	{
		return DB::table('enroll')->where('id', $id)->first();
	}

}