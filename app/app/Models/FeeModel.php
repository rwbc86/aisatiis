<?php namespace App\Models;

use DB;

class FeeModel {

	public static function getAllActive()
	{
		return DB::table('fees')
		->where('is_active',1)
		->where('id','<>',1)
		->get();
	}

	public static function updateTuitionFee($enroll_id)
	{
		$tuition_fee = DB::table('enroll_class AS a')
							->selectRaw('sum(b.unit_fee) AS total')
							->join('class AS b','a.class_id','=','b.id')
							->where('a.enroll_id', $enroll_id)
							->pluck('total');

		$exist = DB::table('enroll_fees')
					->where('enroll_id', $enroll_id)
					->where('fee_id', 1)
					->get();
		if($exist){
			DB::table('enroll_fees')
					->where('enroll_id', $enroll_id)
					->where('fee_id', 1)
					->update(['amount_due' => $tuition_fee]);
		} else {
			DB::table('enroll_fees')
					->insert(['enroll_id' => $enroll_id, 'fee_id' => 1, 'amount_due' => $tuition_fee]);
		}
	}

}