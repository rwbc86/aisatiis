<?php namespace App\Models;

use DB;
use Storage;

class StudentModel {

	public static function getAll()
	{
		return DB::table('students')->get();
	}

	public static function getAllWithPaginate()
	{
		return DB::table('students')->orderBy('id', 'desc')->paginate(16);
	}

	public static function register($input)
	{

	}

	public static function savePhoto($input)
	{
		$pic_bin = base64_decode($input['photo']);
			$filename = strtolower($input['lastname'].$input['firstname']."_".$input['id'].".jpg");
			$filename = str_replace('ñ', '', $filename);
			$filename = str_replace('Ñ', '', $filename);
			$filename = str_replace(' ', '', $filename);
			$path = generateUploadDir($filename);

			$disk = Storage::disk('students');
			if (!$disk->exists($path.$filename))
				$disk->makeDirectory($path);

			$disk->put($path.$filename, $pic_bin);

			DB::table('students')
					->where('id', $input['id'])
					->update(['photo' => $filename]);

			return true;
	}

	public static function getStudentInfo($id)
	{
		return DB::table('students')->where('id', $id)->first();
	}

	public static function getStudentEducationInfo($id)
	{
		return DB::table('students_education')
				->select('education_levels.name as level', 'school.name as school', 'section', 'graduation', 'honors', 'awards')
				->leftJoin('education_levels', 'students_education.education_level', '=', 'education_levels.id')
				->leftJoin('school', 'students_education.school', '=', 'school.id')
				->where('students_education.student', $id)->get();
	}

	public static function getStudentNameAndPhoto($id)
	{
		return DB::table('students')->select('firstname', 'lastname', 'photo', 'gender')->where('id', $id)->first();
	}

}