<?php
function generateUploadDir($file)
{
	$path = substr($file, 0, 1)."/";
	$path .= substr($file, 1, 1)."/";
	$path .= substr($file, 2, 1)."/";

	return $path;
}