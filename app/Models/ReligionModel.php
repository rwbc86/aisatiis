<?php namespace App\Models;

use DB;

class ReligionModel {

	public static function getAll()
	{
		return DB::table('religion')->get();
	}

	public static function getNameById($id)
	{
		return DB::table('religion')->where('id', $id)->pluck('name');
	}

}