<?php namespace App\Models;

use DB;


class ProgramDegreeModel {
	public static function getAll()
	{
		return DB::table('program_degree')->get();
	}
}