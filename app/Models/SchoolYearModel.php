<?php namespace App\Models;

use DB;


class SchoolYearModel {

	public static function getAll($order = "asc")
	{
		return DB::table('school_year')->orderBy('id',$order)->get();
	}

	public static function getAllWithPaginate()
	{
		return DB::table('school_year')->orderBy('id')->paginate(5);
	}

	public static function save($input)
	{
		DB::table('school_year')->insert(['school_year' => $input['school_year']]);
		return true;
	}


}