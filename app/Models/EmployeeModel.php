<?php namespace App\Models;

use DB;


class EmployeeModel {

	public static function getAllFaculty()
	{
		return DB::table('employee')->get();
	}

	public static function getAllWithPaginate($items)
	{
		return DB::table('employee AS a')
		->select('a.id', 'a.idnum', 'a.first_name', 'a.middle_name', 'a.last_name', 'a.email', 'b.name AS position')
		->join('employee_positions AS b', 'b.id', '=', 'a.position')
		->orderBy('id','desc')
		->paginate($items);
	}

	public static function getAllEmployeePositions()
	{
		return DB::table('employee_positions')->get();
	}

	public static function getInfo($id)
	{
		return DB::table('employee')
		->where('id', $id)
		->first();
	}

	public static function save($input)
	{
		DB::table('employee')->insert($input);
		return true;
	}

	public static function delete($id)
	{
		DB::table('employee')->where('id',$id)->delete();
		return true;
	}

	public static function update($input)
	{
		DB::table('employee')
		->where('id', $input['id'])
		->update($input);
		return true;
	}

}