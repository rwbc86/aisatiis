<?php namespace App\Models;

use DB;


class SubjectTypeModel {
	public static function getAll()
	{
		return DB::table('subject_type')->get();
	}
}