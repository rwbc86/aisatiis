<?php namespace App\Models;

use DB;

class CivilStatusModel {

	public static function getNameById($id)
	{
		return DB::table('civil_status')->where('id', $id)->pluck('name');
	}

}