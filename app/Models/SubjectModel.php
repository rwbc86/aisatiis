<?php namespace App\Models;

use DB;


class SubjectModel {

	public static function getAll()
	{
		return DB::table('subjects')->get();
	}

	public static function getAllWithPaginate($items)
	{
		return DB::table('subjects')->orderBy('id','desc')->paginate($items);
	}

	public static function getSubjectInfo($id)
	{
		return DB::table('subjects')->where('id', $id)->first();
	}

	public static function save($input)
	{
		DB::table('subjects')->insert($input);
		return true;
	}

	public static function delete($id)
	{
		DB::table('subjects')->where('id',$id)->delete();
		return true;
	}


}