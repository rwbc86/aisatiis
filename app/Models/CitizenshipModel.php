<?php namespace App\Models;

use DB;

class CitizenshipModel {

	public static function getAll()
	{
		return DB::table('citizenship')->get();
	}

	public static function getNameById($id)
	{
		return DB::table('citizenship')->where('id', $id)->pluck('name');
	}

}