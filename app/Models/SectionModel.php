<?php namespace App\Models;

use DB;


class SectionModel {

	public static function getAllWithPaginate()
	{

		return DB::table('section')
					->select('section.id', 'section.code','school_year.school_year', 'semester.name')
					->join('school_year', 'section.sy', '=', 'school_year.id')
					->join('semester', 'section.semester', '=', 'semester.id')
					->orderBy('section.id', 'desc')->paginate(5);
	}

	public static function getAllActive()
	{
		return DB::table('section')->get();
	}

	public static function save($input)
	{
		DB::table('section')->insert($input);
		return true;
	}


}