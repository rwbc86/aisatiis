@extends('layouts.inner_page')

@section('page_body')
<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<div class="input-group">
                    <input class="form-control" type="text" name="fee" />
					<div class="input-group-btn">
                      	<button type="button" class="btn btn-primary" id="add-subject"><i class="fa fa-plus"></i></button>
                    </div><!-- /btn-group -->
              	</div>
			</div>
			<div class="box-footer">
			</div>
		</div>
	</div>
</div>
@stop

@section('foot')

@stop