@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3>EMPLOYEES</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-12">
						<a href="{{ route('employee.add') }}" id="add" class="btn btn-info btn-flat btn-small pull-right">Add Employee</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<br />
						<table class="table table-striped">
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Position</th>
								<th>&nbsp;</th>
							</tr>
							@foreach($employees as $employee)
							<tr>
								<td>{{ $employee->idnum }}</td>
								<td><span id="delete-data-{{$employee->id}}">{{ $employee->last_name }}, {{ $employee->first_name }} {{ $employee->middle_name }}</td>
								<td>{{ $employee->email }}</td>
								<td>{{ $employee->position }}</td>
								<td>
									<a onclick="delete_confirm({{$employee->id}},'{{ route('employee.delete', $employee->id) }}')" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a>
									<a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-default btn-flat btn-xs pull-right"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-sm-12">
						<div class="pull-right">
							{!! $employees->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop