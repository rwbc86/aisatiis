@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<form action="{{ route('employee.update') }}" method="post">
				<div class="box-header">
					<h3>ADD NEW EMPLOYEE</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
			                      		<label class="control-label">ID Number</label>
			                      		<input type="text" class="form-control" name="employee[idnum]" value="{{ $employee->idnum }}" disabled="disabled">
			                    	</div>
		                    	</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
			                      		<label class="control-label">First Name</label>
			                      		<input type="text" class="form-control" name="employee[first_name]" value="{{ $employee->first_name }}">
			                    	</div>
		                    	</div>
		                    	<div class="col-sm-4">
									<div class="form-group">
			                      		<label class="control-label">Middle Name</label>
			                      		<input type="text" class="form-control" name="employee[middle_name]" value="{{ $employee->middle_name }}">
			                    	</div>
		                    	</div>
		                    	<div class="col-sm-4">
									<div class="form-group">
			                      		<label class="control-label">Last Name</label>
			                      		<input type="text" class="form-control" name="employee[last_name]" value="{{ $employee->last_name }}">
			                    	</div>
		                    	</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
			                      		<label class="control-label">Email</label>
			                      		<input type="email" class="form-control" name="employee[email]" value="{{ $employee->email }}">
			                    	</div>
		                    	</div>
		                    	<div class="col-sm-4">
		                    		<div class="input-group">
										<label class="control-label">Position</label>
										<select class="form-control" name="employee[position]">
											<option value=""></option>
											@foreach($employee_positions as $position)
											<option value="{{ $position->id }}" {{{ $employee->position === $position->id ? 'selected="selected"' : '' }}}>{{ $position->name }}</option>
											@endforeach
										</select>
				                	</div>
		                    	</div>
		                    	<div class="col-sm-4">
		                    		<div class="input-group">
										<label class="control-label">Does he/she teach?</label>
										<br />
										<input type="radio" name="employee[is_teaching]" value="1" {{{ $employee->is_teaching === 1 ? 'checked="checked"' : '' }}}> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="employee[is_teaching]" value="" {{{ $employee->is_teaching === 0 ? 'checked="checked"' : '' }}}> No
				                	</div>
		                    	</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-left">
	                            <input type="submit" class="btn btn-flat btn-success" value="Update">
	                        </div>
	                        <div class="pull-right">
	                            <a href="{{ route('employee') }}" class="btn btn-flat btn-danger">Cancel</a>
	                        </div>
						</div>
					</div>
				</div>
				<input type="hidden" name="employee[id]" value="{{ $employee->id }}" />
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div>
	</div>
</div>

@stop