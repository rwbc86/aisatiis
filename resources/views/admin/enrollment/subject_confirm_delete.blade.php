@extends('layouts.inner_page')

@section('page_body')

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3>Are you sure you want to delete this subject?</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            {{ $subject->code }}
                            <br />
                            {{ $subject->name }}
                            <br />
                            {{ $subject->description }}
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-left">
                                <a href="{{ route('subject.delete', $subject->id) }}" class="btn btn-flat btn-success">Yes</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('subject') }}" class="btn btn-flat btn-danger">No</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop