<div class="box-header">
	<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Add Program</a>
</div>
<div class="box-body">
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>Code</th>
			<th>Name</th>
			<th>Specialization</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
		@foreach($programs as $program)
		<tr>
			<td>{{$program->id}}</td>
			<td>{{$program->code}}</td>
			<td>{{$program->name}}</td>
			<td>{{$program->specialization}}</td>
			<td>{{$program->is_active}}</td>
			<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
		</tr>
		@endforeach
	</table>
</div>
<div class="box-footer">
	<div class="pull-right">
		{!! $programs->render() !!}
	</div>
</div>