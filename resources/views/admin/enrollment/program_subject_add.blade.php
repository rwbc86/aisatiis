@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<h2>{{ $program->name }}</h2>
			</div>
			<div class="box-body">
				<form action="{{ route('program.subject.save') }}" method="post">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
				                      	<label class="control-label">Subject</label>
										<select class="form-control" name="program_subject[subject]">
											<option value=""></option>
											@foreach ($subjects as $subject)
												<option value="{{ $subject->id }}">{{ $subject->name }} ( {{ $subject->code }} )</option>
											@endforeach
										</select>
				                    </div>
			                    </div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12"><strong>Year</strong></div>
									</div>
									<br />
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[year_level]" value="1" /> 1st</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[year_level]" value="2" /> 2nd</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[year_level]" value="3" /> 3rd</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[year_level]" value="4" /> 4th</div>
									</div>	
								</div>
								<div class="col-sm-4">
									<div class="row">
										<div class="col-sm-12"><strong>Semester</strong></div>
									</div>
									<br />
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[semester]" value="1" /> 1st</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[semester]" value="2" /> 2nd</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[semester]" value="3" /> 3rd</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12"><input type="radio" name="program_subject[semester]" value="4" /> Sum</div>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="pull-right">
										<input type="submit" class="btn btn-primary btn-flat" value="Save" />
										<a href="{{ route('program.edit', $program->id) }}" class="btn btn-danger btn-flat">Cancel</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="program_subject[program]" value="{{ $program->id }}" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				</form>
			</div> <!-- .END BOX-BODY-->
			<div class="box-footer">
				<div class="pull-right">
				
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('foot')
<script type="text/javascript">
	$(function (){
		
		$('input[type="radio"], input[type="checkbox"]').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });
	    
	});
</script>
@stop