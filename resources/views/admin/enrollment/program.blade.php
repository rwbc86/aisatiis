@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Add Program</a>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tr>
						<th>#</th>
						<th>Code</th>
						<th>Name</th>
						<th>Specialization</th>
						<th>Status</th>
						<th>&nbsp;</th>
					</tr>
					@foreach($programs as $program)
					<tr>
						<td>{{$program->id}}</td>
						<td>{{$program->code}}</td>
						<td>{{$program->name}}</td>
						<td>{{$program->specialization}}</td>
						<td>{{$program->is_active}}</td>
						<td>
							<a href="{{ route('program.edit', ['id' => $program->id]) }}" class="btn btn-warning btn-flat btn-xs"><i class="fa fa-edit"></i></a>
							<a href="#" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					{!! $programs->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('modals')

<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Add Program</h4>
			</div>
			<form id="add-program">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 form-group">
						<label class="control-label">Code</label>
						<br />
						<input type="text" class="form-control" placeholder="Code" name="program[code]" value="{{ old('program.code') }}" />
					</div>
					<div class="col-md-4 form-group">
						<label class="control-label">Degree</label>
						<br />
						<select class="form-control" name="program[degree]">
							<option value=""></option>
							@foreach ($degrees as $degree)
								<option value="{{ $degree->id }}" {{ ($degree->id == old('program.degree'))? 'selected' : '' }}>{{ $degree->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3 orm-group">
						<label class="control-label">Year</label>
						<br />
						<input type="text" class="form-control" placeholder="Curriculum Year" name="program[curriculum_year]" value="{{ old('program.year') }}" />
					</div>
					<div class="col-md-3 form-group">
						<label class="control-label">Max Students</label>
						<br />
						<input type="text" class="form-control" placeholder="Max Students" name="program[max_students]" value="{{ old('program.max') }}" />
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="control-label">Program Name</label>
					<br />
					<input type="text" class="form-control" placeholder="Program Name" name="program[name]" value="{{ old('program.name') }}" />
				</div>
				<div class="form-group">
					<label class="control-label">Specialization</label>
					<br />
					<input type="text" class="form-control" placeholder="Specialization" name="program[specialization]" value="{{ old('program.specialization') }}" />
				</div>
				<div class="form-group">
					<label class="control-label">Active?</label>
					<br />	                  
					<input type="radio" value="1" name="program[is_active]" {{ ("m"==old('program.is_active'))? 'checked' : '' }} /> Yes
					&nbsp;
					<input type="radio" value="0" name="program[is_active]" {{ ("f"==old('program.is_active'))? 'checked' : '' }} /> No
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary btn-flat" id="submit">Save</button>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

@stop

@section('foot')

<script type="text/javascript">
	$(function (){
		$('button#submit').click(function (){
			$.ajax({
				method: 'POST',
				url: '{{ route("program.save") }}',
				data: $('form#add-program').serializeArray()
			}).done(function (msg){
				console.log(msg);
				notify = msg.split('|');
			    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
			    $('.box').load("{{ route('program.ajax.all')}}");
			});
			$('form#add-program')[0].reset()
			$('#modal').modal('hide');
		});

		$('input[type="radio"], input[type="checkbox"]').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });

	});
</script>

@stop