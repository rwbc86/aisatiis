@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>Program Name</th>
								<td>{{ $program->name }}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<a href="{{ route('program.subject.add', $program->id) }}" class="btn btn-flat btn-primary">Add Subject</a>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th>Code</th>
								<th>name</th>
								<th>units</th>
								<th>active</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="pull-right">
				
				</div>
			</div>
		</div>
	</div>
</div>

@stop