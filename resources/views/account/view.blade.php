@extends('layouts.inner_page')

@section('head')

<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-2">
						Name of Student:
					</div>
					<div class="col-sm-8 form-group">
						{{ $student->lastname }}, {{$student->firstname}}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-sm-12">
								<div class="input-group">
				                    <select class="form-control" id="fees" name="program[degree]">
										<option value=""></option>
										@foreach($fees as $fee)
											<option value="{{ $fee->id}}">{{ $fee->name }} ({{$fee->amount}})</option>
										@endforeach
									</select>
									<div class="input-group-btn">
				                      	<button type="button" class="btn btn-primary" id="add-fee"><i class="fa fa-plus"></i></button>
				                    </div><!-- /btn-group -->
				              	</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12" id="account-fees">
								<table class="table table-border">
									<tr>
										<th>Fee</th>
										<th>Amount</th>
									</tr>
									@foreach ($account_fees as $fee)
									<tr>
										<td>{{ $fee->name }}</td>
										<td>{{ $fee->amount_due }}</td>
									</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">

			</div>
		</div>
	</div>
</div>

@stop

@section('foot')
@parent
<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/autocomplete-combo.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$(function (){
			$( "#fees" ).combobox({placeholder: 'Select Fee'});

	});

	$("#add-fee").click(function (){
    	$.ajax({
			method: 'POST',
			url: '{{ route("account.ajax.add.fee") }}',
			data: { fee: { enroll_id:  "{{ $account->id }}", fee_id: $( "#fees" ).val() }, _token: "{{ csrf_token() }}" }
		}).done(function (msg){
			console.log(msg);
			notify = msg.split('|');
		    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
		    $('#account-fees').load("{{ route('account.ajax.list.fees', $account->id)}}");
		});
		$( ".custom-combobox-input" ).val("");
    });

</script>

@endsection