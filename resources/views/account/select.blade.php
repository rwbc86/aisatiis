@extends('layouts.inner_page')

@section('head')

<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<h3>EXISTING ACCOUNT</h3>
						<div class="input-group">
							<select class="form-control" id="accounts">
								<option value=""></option>
								@foreach($accounts as $account)
									<option value="{{ $account->id }}">{{ $account->id }} - [{{ $account->id_number }}] {{ $account->lastname }}, {{ $account->firstname }}</option>
								@endforeach
							</select>
							<div class="input-group-btn">
			                  	<button type="button" class="btn btn-primary" id="go">GO</button>
			                </div>
						</div>
		            </div>
				</div>
			</div>
			<div class="box-footer">

			</div>
		</div>
	</div>
</div>

@stop

@section('foot')
@parent
<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/autocomplete-combo.js') }}" type="text/javascript"></script>

<script type="text/javascript">
	$(function (){
			$( "#accounts" ).combobox({placeholder: 'Select Account'});  
	});
	$('#go').click(function (){ 
		var url = '{{ route("account.view", ":id") }}';
		url = url.replace(':id', $('#accounts').val());
		window.location.href = url ;
	});
</script>

@endsection