@extends('layouts.master')

@section('head')
	<link href="{{ asset('assets/css/internal-common.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('body')
<body class="skin-black fixed">

	<div class="wrapper">

		<header class="main-header">
			<a href="{{ route('index') }}" class="logo"><img src="{{ asset('assets/img/logo2.png') }}" alt="AISAT Logo" class="logo-img" /></a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{{ asset('assets/img/avatar04.png') }}" class="user-image" alt="User Image"/>
								<span class="hidden-xs">{{ Auth::user()->username }}</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="{{ asset('assets/img/avatar04.png') }}" class="img-circle" alt="User Image" />
									<p>
										{{ Auth::user()->username }}
										<small>Member since Nov. 2012</small>
									</p>
								</li>
								<!-- Menu Body -->
								<li class="user-body">
									<div class="col-xs-4 text-center">
										<a href="#">Settings</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">&nbsp;</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">&nbsp;</a>
									</div>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>  
									</div>
									<div class="pull-right">
										<a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{{ asset('assets/img/avatar04.png') }}" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>{{ Auth::user()->username }}</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li>
						<a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
					</li>
					<li class="treeview {{ in_array('Accounting', $breadcrumbs)? 'active':'' }}">
						<a href="#">
							<i class="fa fa-money"></i> <span>Accounting</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="{{ route('transaction.select') }}"><i class="fa fa-circle-o"></i>New Transaction</a></li>
						</ul>
					</li>
					<li class="treeview {{ in_array('students', $breadcrumbs)? 'active':'' }}">
						<a href="{{ route('student') }}">
							<i class="fa fa-graduation-cap"></i> <span>Students</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="{{ route('student') }}"><i class="fa fa-circle-o"></i>List Students</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i>Add New Student</a></li>
						</ul>
					</li>

					<!-- ADMINISTRATION MENU -->
					@if ( in_array( Auth::user()->user_type, array(1) ) )
					<li class="treeview {{ in_array('students', $breadcrumbs)? 'active':'' }}">
						<a href="{{ route('student') }}">
							<i class="fa fa-graduation-cap"></i> <span>Employees</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="{{ route('employee') }}"><i class="fa fa-circle-o"></i>List Employees</a></li>
							<li><a href="{{ route('employee.add') }}"><i class="fa fa-circle-o"></i>Add New Employee</a></li>
						</ul>
					</li>
					<li class="treeview {{ in_array('admin', $breadcrumbs)? 'active':'' }}">
						<a href="#">
							<i class="fa fa-cogs"></i> <span>Administration</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li class="{{ in_array('Accounting', $breadcrumbs)? 'active':'' }}">
						  		<a href="#"><i class="fa fa-circle-o"></i> Accounting <i class="fa fa-angle-left pull-right"></i></a>
						  		<ul class="treeview-menu">
						  			<li class="{{ in_array('Fees', $breadcrumbs)? 'active':'' }}">
								      <a href="#"><i class="fa fa-circle-o"></i> Fees</a>
								    </li>
								    <li class="{{ in_array('Fees Category', $breadcrumbs)? 'active':'' }}">
								      <a href="#"><i class="fa fa-circle-o"></i> Fees Category</a>
								    </li>
						  		</ul>
							</li>
							<li class="{{ in_array('list values', $breadcrumbs)? 'active':'' }}">
						  		<a href="#"><i class="fa fa-circle-o"></i> List Values <i class="fa fa-angle-left pull-right"></i></a>
						  		<ul class="treeview-menu">
						  			<li class="{{ in_array('education levels', $breadcrumbs)? 'active':'' }}">
								      <a href="#"><i class="fa fa-circle-o"></i> Education Levels</a>
								    </li>
						    		<li class="{{ in_array('schools', $breadcrumbs)? 'active':'' }}">
						      			<a href="#"><i class="fa fa-circle-o"></i> Schools</a>
						    		</li>
						  		</ul>
							</li>
							<li class="{{ in_array('list values', $breadcrumbs)? 'active':'' }}">
						  		<a href="#"><i class="fa fa-circle-o"></i> Enrollment <i class="fa fa-angle-left pull-right"></i></a>
						  		<ul class="treeview-menu">
						    		<li class="{{ in_array('school year', $breadcrumbs)? 'active':'' }}">
								      <a href="{{ route('school_year') }}"><i class="fa fa-circle-o"></i> School Year</a>
								    </li>
								    <li class="{{ in_array('period', $breadcrumbs)? 'active':'' }}">
								      <a href="{{ route('section') }}"><i class="fa fa-circle-o"></i> Section</a>
								    </li>
								    <li class="{{ in_array('program', $breadcrumbs)? 'active':'' }}">
								      <a href="{{ route('program') }}"><i class="fa fa-circle-o"></i> Programs</a>
								    </li>
								    <li class="{{ in_array('subject', $breadcrumbs)? 'active':'' }}">
								      <a href="{{ route('subject') }}"><i class="fa fa-circle-o"></i> Subjects</a>
								    </li>
						  		</ul>
							</li>
						</ul>
					</li>

					@endif
					
					<li><a href="#"><i class="fa fa-book"></i> Documentation</a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

	  	<div class="content-wrapper">

	  		@if( isset($title) && isset($subtitle) )
		    	<section class="content-header">
					<h1>{{ $title or '' }}<small>{{ $subtitle or '' }}</small></h1>
				</section>
			@endif
			
	    	<section class="content">
	    		@yield('page_body')
	    	</section><!-- /.content -->

	  	</div><!-- /.content-wrapper -->

	  	<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>&copy;2014 AISAT Integrated Information System | <a href="http://www.ronicconcepcion.com/">RWC</a>
		</footer>
	</div><!-- ./wrapper -->
@stop

@section('foot')
	<script src="{{ asset('assets/js/internal-common.js') }}" type="text/javascript"></script>
@stop