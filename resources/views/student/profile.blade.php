@extends('layouts.inner_page')

@section('page_body')

<div class="box">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-2">
				<a href="{{ route('student.profile.photo.edit', $student->id) }}">
					<img src="{{ (""==$student->photo)? asset('assets/img/'.$student->gender.'nophoto.jpg') : asset('photos/' . $student->photo) }}" width="150" />
				</a>
			</div>
			<div class="col-sm-10">
				<div class="row">
					<div class="col-sm-12">
						<h2>{{ strtoupper($student->lastname) }}, {{ strtoupper($student->firstname) }} {{ strtoupper($student->middlename) }}</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						{{ $student->address }}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<small class="label bg-gray"><i class="fa fa-gift"></i> {{ date_format(date_create_from_format('Y-m-d', $student->birth_date), 'M j, Y') }}</small>
						@if ($student->gender == 'm')
							<small class="label bg-aqua"><i class="fa fa-venus-mars"></i> {{ strtoupper($student->gender) }}ale</small>
						@else
							<small class="label bg-fuchsia"><i class="fa fa-venus-mars"></i> {{ strtoupper($student->gender) }}emale</small>
						@endif
						<small class="label bg-gray">{{ $student->civil_status }}</small>
						<small class="label bg-gray"><i class="fa fa-flag"></i> {{ $student->citizenship }}</small>
						<small class="label bg-gray">{{ $student->religion }}</small>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-12">
						<h2 class="label bg-red">Enrolled</h2> <strong>BS Aircraft Maintenance Technology</strong>
					</div>
				</div>
			</div>
		</div>
		<br />
		<br />
		<div class="row">
			<div class="col-sm-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#personal" data-toggle="tab" aria-expanded="true">Personal</a></li>
						<li class=""><a href="#family" data-toggle="tab" aria-expanded="false">Family</a></li>
						<li class=""><a href="#education" data-toggle="tab" aria-expanded="false">Education</a></li>
						<li class=""><a href="#enrollment" data-toggle="tab" aria-expanded="false">Enrollment</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="personal">
							Personal Information
							<table>
								<tr>
									<td width="200">ID Number</td>
									<td>{{$student->id_number}}</td>
								</tr>
								<tr>
									<td>Birthplace</td>
									<td>{{$student->birth_place}}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>{{$student->email}}</td>
								</tr>
								<tr>
									<td>landline</td>
									<td>{{$student->landline}}</td>
								</tr>
								<tr>
									<td>Mobile</td>
									<td>{{$student->mobile}}</td>
								</tr>
								<tr>
									<td>Company Name</td>
									<td>{{$student->company_name}}</td>
								</tr>
								<tr>
									<td>Company Address</td>
									<td>{{$student->company_address}}</td>
								</tr>
								<tr>
									<td>Position</td>
									<td>{{$student->position}}</td>
								</tr>
								<tr>
									<td>Employment Status</td>
									<td>{{$student->employment_status}}</td>
								</tr>
								<tr>
									<td>Company Landline</td>
									<td>{{$student->company_landline}}</td>
								</tr>
								<tr>
									<td>Company Mobile</td>
									<td>{{$student->company_mobile}}</td>
								</tr>
								<tr>
									<td>Company Fax</td>
									<td>{{$student->company_fax}}</td>
								</tr>
								<tr>
									<td>Emergency Name</td>
									<td>{{$student->emergency_name}}</td>
								</tr>
								<tr>
									<td>Emergency Relationship</td>
									<td>{{$student->emergency_relationship}}</td>
								</tr>
								<tr>
									<td>Emergency Address</td>
									<td>{{$student->emergency_address}}</td>
								</tr>
								<tr>
									<td>Emergency landline</td>
									<td>{{$student->emergency_landline}}</td>
								</tr>
								<tr>
									<td>Emergency Mobile</td>
									<td>{{$student->emergency_mobile}}</td>
								</tr>
							</table>
						</div>
						<div class="tab-pane" id="family">
							<table>
								<tr>
									<td width="200">Mother's Name</td>
									<td>{{$student->mom_name}}</td>
								</tr>
								<tr>
									<td>Mother's Occupation</td>
									<td>{{$student->mom_occupation}}</td>
								</tr>
								<tr>
									<td>Mother's Company</td>
									<td>{{$student->mom_company}}</td>
								</tr>
								<tr>
									<td>Mother's Company Address</td>
									<td>{{$student->mom_company_address}}</td>
								</tr>
								<tr>
									<td>Mother's Landline</td>
									<td>{{$student->mom_landline}}</td>
								</tr>
								<tr>
									<td>Mother's Mobile</td>
									<td>{{$student->mom_mobile}}</td>
								</tr>
								<tr>
									<td>Mother's Fax</td>
									<td>{{$student->dad_fax}}</td>
								</tr>
								<tr>
									<td>Father's Name</td>
									<td>{{$student->dad_name}}</td>
								</tr>
								<tr>
									<td>Father's Occupation</td>
									<td>{{$student->dad_occupation}}</td>
								</tr>
								<tr>
									<td>Father's Company</td>
									<td>{{$student->dad_company}}</td>
								</tr>
								<tr>
									<td>Father's Company Address</td>
									<td>{{$student->dad_company_address}}</td>
								</tr>
								<tr>
									<td>Father's landline</td>
									<td>{{$student->dad_landline}}</td>
								</tr>
								<tr>
									<td>Father's Mobile</td>
									<td>{{$student->dad_mobile}}</td>
								</tr>
								<tr>
									<td>Father's Fax</td>
									<td>{{$student->dad_fax}}</td>
								</tr>
								<tr>
									<td>Parents Address</td>
									<td>{{$student->parent_address}}</td>
								</tr>
								<tr>
									<td>Spouse Name</td>
									<td>{{$student->spouse_name}}</td>
								</tr>
								<tr>
									<td>Spouse Occupation</td>
									<td>{{$student->spouse_occupation}}</td>
								</tr>
								<tr>
									<td>Spouse Company</td>
									<td>{{$student->spouse_company}}</td>
								</tr>
								<tr>
									<td>Spouse Company Address</td>
									<td>{{$student->spouse_company_address}}</td>
								</tr>
								<tr>
									<td>Spouse landline</td>
									<td>{{$student->spouse_landline}}</td>
								</tr>
								<tr>
									<td>Spouse fax</td>
									<td>{{$student->spouse_fax}}</td>
								</tr>
								<tr>
									<td>Spouse Mobile</td>
									<td>{{$student->spouse_mobile}}</td>
								</tr>
								<tr>
									<td>Children</td>
									<td>{{$student->children}}</td>
								</tr>
								<tr>
									<td>Siblings</td>
									<td>{{$student->siblings}}</td>
								</tr>
								<tr>
									<td>Family Income</td>
									<td>{{$student->family_income}}</td>
								</tr>
							</table>
						</div>
						<div class="tab-pane" id="education">
							<table>
								<tr>
									<th width="200">Education Level</th>
									<th width="300">School</th>
									<th width="100">Section</th>
									<th width="100">Graduation</th>
									<th width="200">Honors</th>
									<th width="200">Awards</th>
								</tr>
							@foreach ($student_educ as $education)	
								<tr>
									<td>{{$education->level}}</td>
									<td>{{$education->school}}</td>
									<td>{{$education->section}}</td>
									<td>{{$education->graduation}}</td>
									<td>{{$education->honors}}</td>
									<td>{{$education->awards}}</td>
								</tr>
							@endforeach
							</table>
						</div>
						<div class="tab-pane" id="enrollment">
							<a href="{{ route('student.enroll', array('id' => $student->id)) }}" class="btn btn-info pull-right">Enroll</a>
							<div class="clearfix"></div>
							<div class="row">
								<div class="col-sm-10 col-sm-offset-1">
									<table class="table table-bordered">
										<tr>
											<th>PROGRAM</th>
											<th>SCHOOL YEAR</th>
											<th>SEMESTER</th>
											<th>STATUS</th>
											<th>&nbsp;</th>
										</tr>
										@foreach($enrolls as $enroll)
										<tr>
											<td>{{ $enroll->program }}</td>
											<td>{{ $enroll->school_year }}</td>
											<td>{{ $enroll->semester }}</td>
											<td>{{ $enroll->is_enrolled }}</td>
											<td>
												<a href="{{ route('student.enroll.subjects',$enroll->id) }}" class="btn btn-flat btn-default">Add Subjects</a>
											</td>
										</tr>
										@endforeach
									</table>
								</div>
							</div>
						</div>
					</div><!-- /.tab-content -->
				</div>
			</div>
		</div>
	</div>
</div>

@stop
