@extends('layouts.inner_page')

@section('head')
@parent
<link href="{{ asset('assets/css/register.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('page_body')

<body class="register-page">
	<div class="row">
	  	<div class="col-sm-12">
	    	<div class="register-box-body">
	      		<h3>{{ $student->id_number }}</h3>
	      		<br />
				<form action="{{ route('student.update') }}" method="post">
					<h4>Personal Data</h4>
					<div class="box">
						<div class="box-body">
							<div class="row">
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.lastname'))? 'has-error' : '' }}">
											<label class="control-label">Last Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Last Name" name="student[lastname]" value="{{ $student->lastname }}" />
										</div>
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.firstname'))? 'has-error' : '' }}">
											<label class="control-label">First Name</label>
											<br />
											<input type="text" class="form-control" placeholder="First Name" name="student[firstname]" value="{{ $student->firstname }}" />
										</div>
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.middlename'))? 'has-error' : '' }}">
											<label class="control-label">Middle Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Middle Name" name="student[middlename]" value="{{ $student->middlename }}"/>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 form-group {{ ('' !== $errors->first('student.address'))? 'has-error' : '' }}">
											<label class="control-label">Present Address</label>
											<br />
											<input type="text" class="form-control" placeholder="Present Address" name="student[address]" value="{{ $student->address }}" />
										</div>
									</div>
								</div>
								<div class="col-sm-2" id="camera-area">
									<!--@if (""!=$student->photo)
										<img src="{{ $student->photo }}" alt="Profile Photo" width="150" />
									@else
										<div id="my-camera"></div>
									@endif-->
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.birth_date'))? 'has-error' : '' }}">
									<label class="control-label">Birth Date</label>
									<br />
									<input type="text" class="form-control" placeholder="Birth Date" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask id="datemask"  name="student[birth_date]" value="{{ $student->birth_date }}" />
								</div>
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.birth_place'))? 'has-error' : '' }}">
									<label class="control-label">Birth Place</label>
									<br />
									<input type="text" class="form-control" placeholder="Birth Place" name="student[birth_place]" value="{{ $student->birth_place }}" />
								</div>
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.gender'))? 'has-error' : '' }}">
									<label class="control-label">Gender</label>
									<br />	                  
									<input type="radio" value="m" class="gender" name="student[gender]" {{ ("m"==$student->gender)? 'checked' : '' }} /> Male
									&nbsp;
									<input type="radio" value="f" class="gender" name="student[gender]" {{ ("f"==$student->gender)? 'checked' : '' }} /> Female
								</div>
								<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.civil_status'))? 'has-error' : '' }}">
									<label>Civil Status</label>
									<br />
									<input type="radio" value="1" class="civil-status" name="student[civil_status]" {{ ("1"==$student->civil_status)? 'checked' : '' }} /> Single
									&nbsp;
									<input type="radio" value="2" class="civil-status" name="student[civil_status]" {{ ("2"==$student->civil_status)? 'checked' : '' }} /> Married
									&nbsp;
									<input type="radio" value="3" class="civil-status" name="student[civil_status]" {{ ("3"==$student->civil_status)? 'checked' : '' }} /> Widowed
									&nbsp;
									<input type="radio" value="4" class="civil-status" name="student[civil_status]" {{ ("4"==$student->civil_status)? 'checked' : '' }} /> Separated
								</div>
								<div class="col-sm-2" id="camera-controls">
									<!--<button type="button" class="btn btn-primary btn-flat btn-block" id="camera-capture" style="{{(""==$student->photo)? '' : 'display:none;'}}" >
										<i class="fa fa-camera"></i>&nbsp;&nbsp;Capture Photo
									</button>
									<button type="button" class="btn btn-danger btn-flat pull-left" id="camera-reset" style="display:none;">
										<i class="fa fa-undo"></i> Reset
									</button>
									<button type="button" class="btn btn-success btn-flat pull-right" id="camera-save" style="display:none;">
										<i class="fa fa-save"></i> Save
									</button>
									<input type="hidden" name="photo_data" id="photo-data" value="{{ old('photo_data') }}"/>
									<input type="hidden" name="photo" id="photo" value="{{ old('photo') }}"/>-->
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.citizenship'))? 'has-error' : '' }}">
									<label class="control-label">Citizenship</label>
									<br />
									<div class="input-group">
										<select class="form-control" id="citizenship" name="student[citizenship]">
											<option value=""></option>
											@foreach ($citizenships as $citizenship)
												<option value="{{ $citizenship->id }}" {{ ($citizenship->id ==$student->citizenship)? 'selected' : '' }}>{{ $citizenship->name }}</option>
											@endforeach
										</select>
										<div class="input-group-btn">
					                      	<button type="button" class="btn btn-default citizenship"><i class="fa fa-refresh"></i></button>
					                    </div>
				                    </div>
								</div>
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.religion'))? 'has-error' : '' }}">
									<label class="control-label">Religion</label>
									<br />
									<div class="input-group">
										<select class="form-control" id="religion" name="student[religion]">
											<option value=""></option>
											@foreach ($religions as $religion)
												<option value="{{ $religion->id }}" {{ ($religion->id == $student->religion)? 'selected' : '' }}>{{ $religion->name }}</option>
											@endforeach
										</select>
										<div class="input-group-btn">
					                      	<button type="button" class="btn btn-default religion"><i class="fa fa-refresh"></i></button>
					                    </div>
					                </div>
								</div>
								<div class="col-sm-3 form-group {{ ('' !== $errors->first('student.email'))? 'has-error' : '' }}">
									<label class="control-label">Email Address</label>
									<br />
									<input type="text" class="form-control" placeholder="Email Address" name="student[email]" value="{{ $student->email }}" />
								</div>
								<div class="col-sm-2 form-group">
									<label class="control-label">Landline No.</label>
									<br />
									<input type="text" class="form-control" placeholder="Landline No" data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[landline]" value="{{ $student->landline }}"/>
								</div>
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.mobile'))? 'has-error' : '' }}">
									<label class="control-label">Mobile No.</label>
									<br />
									<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[mobile]" value="{{ $student->mobile }}"/>
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-sm-5 form-group {{ ('' !== $errors->first('student.is_employed'))? 'has-error' : '' }}">
									<label class="control-label">Are you currently employed?</label>&nbsp;&nbsp;
									<input type="radio" value="1" class="is-employed" name="student[is_employed]" {{ ('1' == $student->is_employed)? 'checked' : '' }} /> Yes
									&nbsp;
									<input type="radio" value="0" class="is-employed" name="student[is_employed]" {{ ('0' == $student->is_employed)? 'checked' : '' }} /> No
								</div>
							</div>
							<div class="employment-info" {{ ('1' == old('student.is_employed'))? 'style=display:block !important;' : '' }}  >
								<div class="row">
									<div class="col-sm-3 form-group">
										<label class="control-label">Company Name</label>
										<br />
										<input type="text" class="form-control" placeholder="Company Name" name="student[company_name]" value="{{ $student->company_name }}" />
									</div>
									<div class="col-sm-3 form-group">
										<label class="control-label">Position</label>
										<br />
										<input type="text" class="form-control" placeholder="Position" name="student[position]" value="{{ old('student.position') }}"/>
									</div>
									<div class="col-sm-5 form-group">
										<label class="control-label">Employment Status</label>
										<br />
										<input type="radio" value="1" class="flat-red" name="student[employment_status]" {{ ('1' == $student->employment_status)? 'checked' : '' }} /> Permanent
										&nbsp;
										<input type="radio" value="2" class="flat-red" name="student[employment_status]" {{ ('2' == $student->employment_status)? 'checked' : '' }} /> Contractual
										&nbsp;
										<input type="radio" value="3" class="flat-red" name="student[employment_status]" {{ ('3' == $student->employment_status)? 'checked' : '' }} /> Proby
										&nbsp;
										<input type="radio" value="4" class="flat-red" name="student[employment_status]" {{ ('4' == $student->employment_status)? 'checked' : '' }} /> Project Based
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 form-group">
										<label class="control-label">Company Address</label>
										<br />
										<input type="text" class="form-control" placeholder="Company Address" name="student[company_address]" value="{{ $student->company_address }}" />
									</div>
									<div class="col-sm-2 form-group">
										<label class="control-label">Landline No.</label>
										<br />
										<input type="text" class="form-control" placeholder="Landline No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[company_landline]" value="{{ $student->company_landline }}" />
									</div>
									<div class="col-sm-2 form-group">
										<label class="control-label">Fax No.</label>
										<br />
										<input type="text" class="form-control" placeholder="Fax No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[company_fax]" value="{{ $student->company_fax }}" />
									</div>
									<div class="col-sm-2 form-group">
										<label class="control-label">Mobile No.</label>
										<br />
										<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[company_mobile]" value="{{ $student->company_mobile }}" />
									</div>
								</div>
							</div>
						</div><!-- END BOX BODY -->
					</div><!-- END BOX -->
					
					<h4>Family Data</h4>
					<div class="box">
						<div class="box-body">
							<div class="row">
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.mom_name'))? 'has-error' : '' }}">
											<label class="control-label">Mother's Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Mother's Name" name="student[mom_name]" value="{{ $student->mom_name }}" />
										</div>
										<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.mom_mobile'))? 'has-error' : '' }}">
											<label class="control-label">Mobile No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[mom_mobile]" value="{{ $student->mom_mobile }}" />
										</div>
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.dad_name'))? 'has-error' : '' }}">
											<label class="control-label">Father's Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Father's Name" name="student[dad_name]" value="{{ $student->dad_name }}" />
										</div>
										<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.dad_mobile'))? 'has-error' : '' }}">
											<label class="control-label">Mobile No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[dad_mobile]" value="{{$student->dad_mobile }}" />
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 form-group">
											<label class="control-label">Company Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Company Name" name="student[mom_company]" value="{{ $student->mom_company }}" />
										</div>
										<div class="col-sm-6 form-group">
											<label class="control-label">Company Name</label>
											<br />
											<input type="text" class="form-control" placeholder="Company Name" name="student[dad_company]" value="{{ $student->dad_company }}" />
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4 form-group">
											<label class="control-label">Company Address</label>
											<br />
											<input type="text" class="form-control" placeholder="Company Address" name="student[mom_company_address]" value="{{ $student->mom_company_address }}" />
										</div>
										<div class="col-sm-2 form-group">
											<label class="control-label">Fax No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Fax No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[mom_fax]" value="{{ $student->mom_fax }}" />
										</div>
										<div class="col-sm-4 form-group">
											<label class="control-label">Company Address</label>
											<br />
											<input type="text" class="form-control" placeholder="Company Address" name="student[dad_company_address]" value="{{ $student->dad_company_address }}" />
										</div>
										<div class="col-sm-2 form-group">
											<label class="control-label">Fax No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Fax No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[dad_fax]" value="{{ $student->dad_fax }}" />
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.mom_occupation'))? 'has-error' : '' }}">
											<label class="control-label">Occupation</label>
											<br />
											<input type="text" class="form-control" placeholder="Occupation" name="student[mom_occupation]" value="{{ $student->mom_occupation }}" />
										</div>
										<div class="col-sm-2 form-group">
											<label class="control-label">Landline No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Landline No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[mom_landline]" value="{{ $student->mom_landline }}" />
										</div>
										<div class="col-sm-4 form-group {{ ('' !== $errors->first('student.dad_occupation'))? 'has-error' : '' }}">
											<label class="control-label">Occupation</label>
											<br />
											<input type="text" class="form-control" placeholder="Occupation" name="student[dad_occupation]" value="{{ $student->dad_occupation }}" />
										</div>
										<div class="col-sm-2 form-group">
											<label class="control-label">Landline No.</label>
											<br />
											<input type="text" class="form-control" placeholder="Landline No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[dad_landline]" value="{{ $student->dad_landline }}" />
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group {{ ('' !== $errors->first('student.family_income'))? 'has-error' : '' }}">
										<label class="control-label">Monthly Family Income</label>
										<br />
										<input type="radio" value="F" class="flat-red" name="student[family_income]" {{ ('F' == $student->family_income)? 'checked' : '' }} /> Below 10,000
										<br /><br />
										<input type="radio" value="E" class="flat-red" name="student[family_income]" {{ ('E' == $student->family_income)? 'checked' : '' }} /> 10,001 - 20,000
										<br /><br />
										<input type="radio" value="D" class="flat-red" name="student[family_income]" {{ ('D' == $student->family_income)? 'checked' : '' }} /> 20,001 - 30,000
										<br /><br />
										<input type="radio" value="C" class="flat-red" name="student[family_income]" {{ ('C' == $student->family_income)? 'checked' : '' }} /> 30,001 - 40,000
										<br /><br />
										<input type="radio" value="B" class="flat-red" name="student[family_income]" {{ ('B' == $student->family_income)? 'checked' : '' }} /> 40,001 - 50,000
										<br /><br />
										<input type="radio" value="A" class="flat-red" name="student[family_income]" {{ ('A' == $student->family_income)? 'checked' : '' }} /> Above 50,000
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-12 form-group {{ ('' !== $errors->first('student.parent_address'))? 'has-error' : '' }}">
											<label class="control-label">Parent's Address</label>
											<br />
											<input type="text" class="form-control" placeholder="Parent's Address" name="student[parent_address]" value="{{ $student->parent_address }}" />
										</div>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group {{ ('' !== $errors->first('student.siblings'))? 'has-error' : '' }}">
										<label class="control-label">No. of Siblings</label>
										<br />
										<input type="text" class="form-control" placeholder="No. of siblings" name="student[siblings]" value="{{ $student->siblings }}" />
									</div>
								</div>
							</div>
							<br />
							<div class="spouse-info"  {{ (in_array(old('student.civil_status'),array(2,3,4)))? 'style=display:block !important;' : '' }} >
								<div class="row">
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-4 form-group">
												<label class="control-label">Spouse Name</label>
												<br />
												<input type="text" class="form-control" placeholder="Spouse Name" name="student[spouse_name]" value="{{ $student->spouse_name }}" />
											</div>
											<div class="col-sm-3 form-group">
												<label class="control-label">Occupation</label>
												<br />
												<input type="text" class="form-control" placeholder="Occupation" name="student[spouse_occupation]" value="{{ $student->spouse_occupation }}" />
											</div>
											<div class="col-sm-5 form-group">
												<label class="control-label">Company Name</label>
												<br />
												<input type="text" class="form-control" placeholder="Company Name" name="student[spouse_company]" value="{{ $student->spouse_company }}" />
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<label class="control-label">No. of Children</label>
											<br />
											<input type="text" class="form-control" placeholder="No. of children" name="student[children]" value="{{ $student->children }}" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-10">
										<div class="row">
											<div class="col-sm-6 form-group">
												<label class="control-label">Company Address</label>
												<br />
												<input type="text" class="form-control" placeholder="Company Address" name="student[spouse_company_address]" value="{{ $student->spouse_company_address }}" />
											</div>
											<div class="col-sm-2 form-group">
												<label class="control-label">Landline No.</label>
												<br />
												<input type="text" class="form-control" placeholder="Landline No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[spouse_landline]" value="{{ $student->spouse_landline }}" />
											</div>
											<div class="col-sm-2 form-group">
												<label class="control-label">Fax No.</label>
												<br />
												<input type="text" class="form-control" placeholder="Fax No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[spouse_fax]" value="{{ $student->spouse_fax }}" />
											</div>
											<div class="col-sm-2 form-group">
												<label class="control-label">Mobile No.</label>
												<br />
												<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[spouse_mobile]" value="{{ $student->spouse_mobile }}" />
											</div>
										</div>
									</div>
									<div class="col-sm-2">
									</div>
								</div>
							</div>
						</div>
					</div>

					<h4>In case of emergency</h4>
					<div class="box">
						<div class="box-body">

							<div class="row">
								<div class="col-xs-9 form-group {{ ('' !== $errors->first('student.emergency_name'))? 'has-error' : '' }}">
									<label class="control-label">Name</label>
									<br />
									<input type="text" class="form-control" placeholder="Name" name="student[emergency_name]" value="{{ $student->emergency_name }}" />
								</div>
								<div class="col-xs-3 form-group {{ ('' !== $errors->first('student.emergency_relationship'))? 'has-error' : '' }}">
									<label class="control-label">Relationship</label>
									<br />
									<input type="text" class="form-control" placeholder="Relationship" name="student[emergency_relationship]" value="{{ $student->emergency_relationship }}" />
								</div>
							</div>

							<div class="row">
								<div class="col-xs-8 form-group {{ ('' !== $errors->first('student.emergency_address'))? 'has-error' : '' }}">
									<label class="control-label">Address</label>
									<br />
									<input type="text" class="form-control" placeholder="Address" name="student[emergency_address]" value="{{ $student->emergency_address }}" />
								</div>
								<div class="col-sm-2 form-group">
									<label class="control-label">Landline No.</label>
									<br />
									<input type="text" class="form-control" placeholder="Landline No." data-inputmask="'mask': '(999) 999-9999'" data-mask name="student[emergency_landline]" value="{{ $student->emergency_landline }}" />
								</div>
								<div class="col-sm-2 form-group {{ ('' !== $errors->first('student.emergency_mobile'))? 'has-error' : '' }}">
									<label class="control-label">Mobile No.</label>
									<br />
									<input type="text" class="form-control" placeholder="Mobile No." data-inputmask="'mask': '0999 999 9999'" data-mask name="student[emergency_mobile]" value="{{ $student->emergency_mobile }}" />
								</div>
							</div>
						</div>
					</div>

					<h4>Educational Background</h4>
					<div class="box">
						<div class="box-body" id="school-list">
							<div class="row">
								<div class="col-xs-12">
									<table class="table table-striped">
										<tbody>
											<tr>  
												<th style="width: 15%">Level</th>
												<th style="width: 30%">School</th>
												<th style="width: 35%">Address</th>
												<th style="width: 10%">Section</th>
												<th style="width: 10%">Year Graduated</th>
											</tr>
											<tr>
												<td>Primary</td>
												<td>
													<div class="input-group">
														<select class="form-control" id="primary-school" name="education[primary_school]">
															<option value="0"></option>
															@foreach ($schools as $school)
																<option value="{{ $school->id }}" {{ ($school->id == old('education.primary_school'))? 'selected' : '' }}>{{ $school->name }}</option>
															@endforeach
														</select>
														<div class="input-group-btn">
									                      	<button type="button" class="btn btn-default school"><i class="fa fa-refresh"></i></button>
									                    </div>
													</div>
												</td>
												<td></td>
												<td><input type="text" class="form-control" placeholder="Section" name="education[primary_section]" value="{{ old('education.primary_section') }}" /></td>
												<td><input type="text" class="form-control" placeholder="Year Graduated" name="education[primary_graduation]" value="{{ old('education.primary_graduation') }}" /></td>
											</tr>
											<tr>
												<td>Intermediate</td>
												<td>
													<div class="form-group input-group {{ ('' !== $errors->first('education.intermediate_school'))? 'has-error' : '' }}">
														<select class="form-control" id="intermediate-school" name="education[intermediate_school]">
															<option value="0"></option>
															@foreach ($schools as $school)
																<option value="{{ $school->id }}" {{ ($school->id == old('education.intermediate_school'))? 'selected' : '' }}>{{ $school->name }}</option>
															@endforeach
														</select>
														<div class="input-group-btn">
									                      	<button type="button" class="btn btn-default school"><i class="fa fa-refresh"></i></button>
									                    </div>
													</div>
												</td>
												<td></td>
												<td><input type="text" class="form-control" placeholder="Section" name="education[intermediate_section]" value="{{ old('education.intermediate_section') }}" /></td>
												<td>
													<div class="form-group {{ ('' !== $errors->first('education.intermediate_graduation'))? 'has-error' : '' }}">
														<input type="text" class="form-control" placeholder="Year Graduated" name="education[intermediate_graduation]" value="{{ old('education.intermediate_graduation') }}" />
													</div>
												</td>
											</tr>
											<tr>
												<td>Secondary</td>
												<td>
												<div class="form-group input-group {{ ('' !== $errors->first('education.secondary_school'))? 'has-error' : '' }}">
													<select class="form-control" id="secondary-school" name="education[secondary_school]">
														<option value="0"></option>
														@foreach ($schools as $school)
															<option value="{{ $school->id }}" {{ ($school->id == old('education.secondary_school'))? 'selected' : '' }}>{{ $school->name }}</option>
														@endforeach
													</select>
													<div class="input-group-btn">
								                      	<button type="button" class="btn btn-default school"><i class="fa fa-refresh"></i></button>
								                    </div>
												</div>
												</td>
												<td></td>
												<td><input type="text" class="form-control" placeholder="Section" name="education[secondary_section]" value="{{ old('education.secondary_section') }}" /></td>
												<td>
													<div class="form-group {{ ('' !== $errors->first('education.secondary_graduation'))? 'has-error' : '' }}">
														<input type="text" class="form-control" placeholder="Year Graduated" name="education[secondary_graduation]" value="{{ old('education.secondary_graduation') }}" />
													</div>
												</td>
											</tr>
											<tr>
												<td>Tertiary</td>
												<td>
													<div class="input-group">
														<select class="form-control" id="tertiary-school" name="education[tertiary_school]">
															<option value="0"></option>
															@foreach ($schools as $school)
																<option value="{{ $school->id }}" {{ ($school->id == old('education.tertiary_school'))? 'selected' : '' }}>{{ $school->name }}</option>
															@endforeach
														</select>
														<div class="input-group-btn">
									                      	<button type="button" class="btn btn-default school"><i class="fa fa-refresh"></i></button>
									                    </div>
								                    </div>
												</td>
												<td></td>
												<td><input type="text" class="form-control" placeholder="Section" name="education[tertiary_section]" value="{{ old('education.tertiary_section') }}" /></td>
												<td><input type="text" class="form-control" placeholder="Year Graduated" name="education[tertiary_graduation]" value="{{ old('education.tertiary_graduation') }}" /></td>
											</tr>
											<tr>
											<td>Vocational</td>
												<td>
													<div class="input-group">
														<select class="form-control" id="vocational-school" name="education[vocational_school]">
														<option value="0"></option>
														s	@foreach ($schools as $school)
																<option value="{{ $school->id }}" {{ ($school->id == old('education.vocational_school'))? 'selected' : '' }}>{{ $school->name }}</option>
															@endforeach
														</select>
														<div class="input-group-btn">
									                      	<button type="button" class="btn btn-default school"><i class="fa fa-refresh"></i></button>
									                    </div>
								                    </div>
												</td>
												<td></td>
												<td><input type="text" class="form-control" placeholder="Section" name="education[vocational_section]" value="{{ old('education.vocational_section') }}" /></td>
												<td><input type="text" class="form-control" placeholder="Year Graduated" name="education[vocational_graduation]" value="{{ old('education.vocational_graduation') }}" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<table class="table table-striped">
										<tbody>
											<tr>  
												<th style="width: 20%">&nbsp;</th>
												<th style="width: 40%">Intermediate</th>
												<th style="width: 40%">Secondary</th>
											</tr>
											<tr>
												<td>Highest Honor Attained</td>
												<td><input type="text" class="form-control" placeholder="Honor" name="education[intermediate_honor]" value="{{ old('education.intermediate_honor') }}" /></td>
												<td><input type="text" class="form-control" placeholder="Honor" name="education[secondary_honor]" value="{{ old('education.secondary_honor') }}" /></td>
											</tr>
											<tr>
												<td>Awards Received</td>
												<td><textarea class="form-control" rows="3" placeholder="Awards Received" name="education[intermediate_awards]" > {{ old('education.intermediate_awards') }} </textarea></td>
												<td><textarea class="form-control" rows="3" placeholder="Awards Received" name="education[secondary_awards]"> {{ old('education.secondary_awards') }} </textarea></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-2 col-xs-offset-10">
							<input type="submit" class="btn btn-primary btn-block btn-flat" id="register-btn" value="Update" />
						</div><!-- /.col -->
					</div>
					<input type="hidden" name="student_id" value="{{ $student->id }}" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				</form>        
			</div><!-- /.register-box-body -->
		</div><!-- /.col -->
	</div><!-- /.row -->

@stop

@section('foot')
@parent
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/webcamjs/webcam.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/autocomplete-combo.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(function (){
	
	$("[data-mask]").inputmask();

	$('input[type="radio"], input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    $('.civil-status').on('ifChecked', function (event) {
		if ($(this).val() >= 2)
    		$('.spouse-info').fadeIn();
		else	
			$('.spouse-info').fadeOut();
    });
    
    $('.is-employed').on('ifChecked', function (event) {
    	if (1 == $(this).val())
    		$('.employment-info').fadeIn();
    	else
    		$('.employment-info').fadeOut();
    });

	$( "#religion" ).combobox({placeholder: 'Religion'});
	$( "#citizenship" ).combobox({placeholder: 'Citizenship'});
	$( "#primary-school" ).combobox({placeholder: 'Primary School'});
	$( "#intermediate-school" ).combobox({placeholder: 'Intermediate School'});
	$( "#secondary-school" ).combobox({placeholder: 'Secondary School'});
	$( "#tertiary-school" ).combobox({placeholder: 'Tertiary School'});
	$( "#vocational-school" ).combobox({placeholder: 'Vocational School'});
});

$('.school').on('click', function (){
	$("#primary-school").load("{{ route('school.ajax.all')}}");
	$("#intermediate-school").load("{{ route('school.ajax.all')}}");
	$("#secondary-school").load("{{ route('school.ajax.all')}}");
	$("#tertiary-school").load("{{ route('school.ajax.all')}}");
	$("#vocational-school").load("{{ route('school.ajax.all')}}");
});
</script>

@stop