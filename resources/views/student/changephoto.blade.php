@extends('layouts.inner_page')

@section('page_body')

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="box box-primary">
				<div class="box-header">
				  <h3 class="box-title">{{ $student->firstname }} {{ $student->lastname }}</h3>
				</div>
				<form action="{{ route('student.profile.photo.save') }}" method="post" id="edit-photo-form">
					<input type="hidden" name="id" id="id" value="{{ $id }}"/>
					<input type="hidden" name="firstname" id="firstname" value="{{ $student->firstname }}"/>
					<input type="hidden" name="lastname" id="lastname" value="{{ $student->lastname }}"/>
					<input type="hidden" name="photo" id="photo" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div class="box-body">
						<div class="row">
							<div class="col-sm-7" id="camera-area">
								<div id="my-camera" style="width: 400px; margin: 0 auto;"></div>
							</div>
							<div class="col-sm-5">
								<img src="{{ (""==$student->photo)? asset('assets/img/'.$student->gender.'nophoto.jpg') : asset('photos/' . $student->photo) }}" width="150" />
							</div>
				  		</div>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-sm-12">
								<button type="button" class="btn btn-primary btn-flat btn-block" id="camera-capture" style="{{(""==old('photo_data'))? '' : 'display:none;'}}" >
									<i class="fa fa-camera"></i>&nbsp;&nbsp;Capture Photo
								</button>
								<button type="button" class="btn btn-danger btn-flat pull-left" id="camera-reset" style="display:none;">
									<i class="fa fa-undo"></i> Reset
								</button>
								<button type="button" class="btn btn-success btn-flat pull-right" id="camera-save" style="display:none;">
									<i class="fa fa-save"></i> Save
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('foot')
<script src="{{ asset('assets/plugins/webcamjs/webcam.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">

Webcam.set({
		// live preview size
		width: 400,
		height: 300,
		
		// device capture size
		dest_width: 400,
		dest_height: 300,
		
		// final cropped size
		crop_width: 300,
		crop_height: 300,
		
		image_format: 'jpeg',
		jpeg_quality: 90
	});
	
	Webcam.attach( '#my-camera' );

	$("#camera-capture").click(function (){
		Webcam.freeze();
		$("#camera-reset").show();
		$("#camera-save").show();
		$(this).hide();
	});

	$("#camera-reset").click( function() {
		Webcam.unfreeze();
		$("#camera-capture").show();
		$("#camera-save").hide();
		$(this).hide();
	});

	$("#camera-save").click( function() {
		$("#camera-reset").hide();
		$("#camera-save").hide();
		$(this).hide();
		Webcam.snap( function(data_uri) {
			var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
			$('#photo').val(raw_image_data);
			$('#edit-photo-form').submit();
		});
	});

</script>

@stop