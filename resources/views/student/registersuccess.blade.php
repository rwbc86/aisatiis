@extends('layouts.master')

@section('head')

<link href="{{ asset('assets/css/register.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('body')
<body class="register-page">
	<div class="row">
	  <div class="col-sm-10 col-sm-offset-1">
	    <div class="login-logo">
	      <img class="logo" alt="AISAT Logo" src="{{ asset('assets/img/logo.png') }}" />
	    </div><!-- /.login-logo -->

	    <div class="register-box-body">
	      <div class="alert alert-success alert-dismissable">
	        <h2><i class="icon fa fa-check"></i> Registration Successful!</h2>
	        <h3>Congratulations! You have been registered to the system successfully!</h3>
	        <br />
	      </div>
	      <br />
	      <a href="{{ route('student.register') }}" class="btn btn-primary btn-flat">New Registration</a>    
	    </div><!-- /.register-box-body -->
	  </div><!-- /.col -->
	</div><!-- /.row -->
	<div class="copyright">&copy;2014 AISAT Integrated Information System | <a href="http://www.ronicconcepcion.com/">RWC</a></div>
@stop