@extends('layouts.inner_page')


@section('head')

<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('page_body')

<div class="box">
	<div class="box-body">
		<div class="row">
			<div class="col-sm-2">
				<img src="{{ asset('assets/img/logo2.png') }}" alt="AISAT Logo" class="logo-img" />
			</div>
			<div class="col-sm-8 text-center">
				<strong>ASIAN INTERNATIONAL SCHOOL OF AERONAUTICS AND TECHNOLOGY</strong><br />
				Sta. Ana Avenue Corner Leon Garcia St., Davao City<br />
				295-7219 / 305-7992
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center">
				<h3>ENROLLMENT FORM</h3>
			</div>
		</div>

		<br />

		<div class="row">
			<div class="col-sm-2 col-sm-offset-1"><strong>Name:</strong></div>
			<div class="col-sm-2" style="border-bottom: 1px solid #000;">{{ strtoupper($student->lastname) }}</div>
			<div class="col-sm-2 col-sm-offset-1 underline">{{ strtoupper($student->firstname) }}</div>
			<div class="col-sm-2 col-sm-offset-1 underline">{{ strtoupper($student->middlename) }}</div>
		</div>

		<div class="row">
			<div class="col-sm-2 col-sm-offset-1">&nbsp;</div>
			<div class="col-sm-2 text-center"><small>SURNAME</small></div>
			<div class="col-sm-2 col-sm-offset-1 text-center"><small>FIRST NAME</small></div>
			<div class="col-sm-2 col-sm-offset-1 text-center"><small>MIDDLE NAME</small></div>
		</div>

		<br />

		<div class="row">
			<div class="col-sm-2 col-sm-offset-1"><strong>Present Address:</strong></div>
			<div class="col-sm-8 underline">{{ $student->address }}</div>
		</div>

		<br />

		<div class="row">
			<div class="col-sm-2 col-sm-offset-1"><strong>Civil Status:</strong></div>
			<div class="col-sm-3 underline">{{ $student->civil_status }}</div>
			<div class="col-sm-2 text-right"><strong>Citizenship:</strong></div>
			<div class="col-sm-3 underline">{{ $student->citizenship }}</div>
		</div>

		<br />

		<div class="row">
			<div class="col-sm-2 col-sm-offset-1"><strong>Email Address:</strong></div>
			<div class="col-sm-3 underline">{{ $student->email }}</div>
			<div class="col-sm-2 text-right"><strong>Contact Number:</strong></div>
			<div class="col-sm-3 underline">{{$student->landline}} / {{ $student->mobile }}</div>
		</div>

		<hr />

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="input-group">
                    <select class="form-control" id="all-classes" name="program[degree]">
						<option value=""></option>
						@foreach($classes as $class)
							<option value="{{ $class->id}}">{{ $class->code }} - {{ $class->name }} ( {{ $class->section }} )</option>
						@endforeach
					</select>
					<div class="input-group-btn">
                      	<button type="button" class="btn btn-primary" id="add-subject"><i class="fa fa-plus"></i></button>
                    </div><!-- /btn-group -->
              	</div>
			</div>
		</div>

		<br />

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1" id="enrolled-subjects">
				<table class="table table-bordered">
					<tr>
						<th colspan="8" class="text-center">CLASS INFORMATION</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>SUBJ. CODE</th>
						<th>SUBJ. DESCRIPTION</th>
						<th>UNIT(S)</th>
						<th>DAY(S)</th>
						<th>TIME</th>
						<th>ROOM</th>
						<th>INSTRUCTOR</th>
					</tr>
					@foreach ($enrolled_classes as $class)
					<tr>
						<td><a onclick="remove_class({{ $class->id }})" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-minus"></i></a></td>
						<td>{{ $class->code }}</td>
						<td>{{ $class->name }}</td>
						<td>{{ $class->units }}</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>{{-- $class->last_name --}}</td>
					</tr>
					@endforeach
					<tr>
						<td colspan="3" class="text-right"><strong>TOTAL # OF UNITS: </strong></td>
						<td colspan="5">{{ $total_units }}</td>
					</tr>
				</table>
			</div>
		</div>

		<hr />

		<div class="row">
			<div class="col-sm-1 col-sm-offset-11">
				<div class="pull-right">
					<a href="{{ route('student') }}" class="btn btn-flat btn-primary">DONE</a>
				</div>
			</div>
		</div>
	</div> <!-- END .BOX-BODY -->
</div> <!-- END .BOX -->
@stop


@section('foot')
@parent
<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/autocomplete-combo.js') }}" type="text/javascript"></script>


<script type="text/javascript">
	$(function (){
			$( "#all-classes" ).combobox({placeholder: 'Select Class to Add'});  
	});

	    $("#add-subject").click(function (){
	    	$.ajax({
				method: 'POST',
				url: '{{ route("student.enroll.class.ajax.add") }}',
				data: { classes: { enroll_id:  "{{ $enroll_session->id }}", student_id: "{{ $enroll_session->student }}", class_id: $( "#all-classes" ).val()}, _token: "{{ csrf_token() }}" }
			}).done(function (msg){
				console.log(msg);
				notify = msg.split('|');
			    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
			    $('#enrolled-subjects').load("{{ route('student.enroll.classes.ajax.all', $enroll_session->id)}}");
			});
			$( ".custom-combobox-input" ).val("");
    });

	function remove_class(id){
		$.ajax({
			method: 'POST',
			url: '{{ route("student.enroll.class.ajax.delete") }}',
			data: { enroll_id:  "{{ $enroll_session->id }}", class_id: id, _token: "{{ csrf_token() }}" }
		}).done(function (msg){
			console.log(msg);
			notify = msg.split('|');
		    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
		    $('#enrolled-subjects').load("{{ route('student.enroll.classes.ajax.all', $enroll_session->id)}}");
		});
	}
</script>
@stop
