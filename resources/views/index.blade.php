@extends('layouts.master')

@section('head')

<link href="{{ asset('assets/css/auth.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('body')

<body class="login-page">

<div class="login-box">
	<div class="login-logo">
		<img class="logo" alt="AISAT Logo" src="assets/img/logo.png" />
	</div><!-- /.login-logo -->
	<div class="login-box-body">
		
		@if (session()->has('alert'))

		<div class="alert alert-{{ session('alert.type') }}">
			<i class="icon fa fa-ban"></i>{{ session('alert.message') }}
		</div>

		@else

		<p class="login-box-msg">Sign in to start your session</p>

		@endif

		<form action="{{ route('login') }}" method="post">
			<div class="form-group has-feedback">
				<input type="text" class="form-control" placeholder="Username" name="username" />
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" class="form-control" placeholder="Password" name="password" />
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">    
					<div class="checkbox icheck">
						<label>
							<input type="checkbox"> Remember Me
						</label>
					</div>                        
				</div>
				<div class="col-xs-4">
					<button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
				</div>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		</form>

	</div><!-- /.login-box-body -->

	<div class="copyright">&copy;2014 AISAT Integrated Information System | <a href="http://www.ronicconcepcion.com/">RWC</a></div>

</div><!-- /.login-box -->

@stop

@section('foot')

<script type="text/javascript">
$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_flat-green',
		increaseArea: '20%' // optional
	});
});
</script>

@stop