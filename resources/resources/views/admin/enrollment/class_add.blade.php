@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">

			</div>

			<div class="box-body">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<form action="{{ route('class.save') }}" method="post">

							<div class="row">
								<div class="col-sm-12">
									<h3>{{ $subject->name }}</h3>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
				                      	<label>School Year</label>
				                      	<select class="form-control" id="school-year" name="class[sy_id]">
											@foreach ($school_years as $school_year)
												<option value="{{ $school_year->id }}">{{ $school_year->school_year }}</option>
											@endforeach
										</select>
				                    </div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
				                      	<label>Semester</label>
				                      	<select class="form-control" id="semester" name="class[semester_id]">
				                      		<option value=""></option>
											@foreach ($semesters as $semester)
												<option value="{{ $semester->id }}">{{ $semester->name }}</option>
											@endforeach
										</select>
				                    </div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
				                      	<label>Section</label>
				                      	<select class="form-control" name="class[section_id]">
				                      		<option value=""></option>
											@foreach ($sections as $section)
												<option value="{{ $section->id }}">{{ $section->code }}</option>
											@endforeach
										</select>
				                    </div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
				                      	<label>Capacity</label>
				                      	<input type="text" class="form-control" placeholder="Capacity" name="class[capacity]">
				                    </div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Instructor</label>
										<select class="form-control" name="class[instructor_id]">
				                      		<option value=""></option>
											@foreach ($faculties as $faculty)
												<option value="{{ $faculty->id }}">{{ $faculty->first_name }} {{ $faculty->last_name }}</option>
											@endforeach
										</select>
				                    </div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4 col-sm-offset-8">
									<div class="pull-right">
										<input type="submit" class="btn btn-primary btn-flat" value="Save" />
										<a href="{{ route('subject.view', $subject->id) }}" class="btn btn-danger btn-flat">Cancel</a>
									</div>
								</div>
							</div>

							<input type="hidden" name="class[subject_id]" value="{{ $subject->id }}" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						</form>
					</div>
				</div>
			</div> <!-- /.box-body -->

			<div class="box-footer">
			</div>
		</div>
	</div>
</div>

@stop

@section('foot')

@stop