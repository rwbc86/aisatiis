@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Add Subject</a>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tr>
						<th>#</th>
						<th>Code</th>
						<th>Name</th>
						<th>Lecture</th>
						<th>Laboratory</th>
						<th>Units</th>
						<th>Type</th>
						<th>&nbsp;</th>
					</tr>
					@foreach($subjects as $subject)
					<tr>
						<td>{{ $subject->id }}</td>
						<td>{{ $subject->code }}</td>
						<td>{{ $subject->name }}</td>
						<td>{{ $subject->lecture }}</td>
						<td>{{ $subject->laboratory }}</td>
						<td>{{ $subject->units }}</td>
						<td>{{ $subject->type }}</td>
						<td>
							<a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a>
							<a href="{{ route('subject.view', $subject->id) }}" class="btn btn-default btn-flat btn-xs pull-right"><i class="fa fa-eye"></i></a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					{!! $subjects->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('modals')

<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Add Subject</h4>
			</div>
			<form id="add-subject">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-2 form-group">
						<label class="control-label">Code</label>
						<br />
						<input type="text" class="form-control" placeholder="Code" name="subject[code]" value="{{ old('subject.code') }}" />
					</div>
					<div class="col-md-4 form-group">
						<label class="control-label">Type</label>
						<br />
						<select class="form-control" name="subject[type]">
							<option value=""></option>
							@foreach ($types as $type)
								<option value="{{ $type->id }}" {{ ($type->id == old('student.type'))? 'selected' : '' }}>{{ $type->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-2 orm-group">
						<label class="control-label">Lecture</label>
						<br />
						<input type="text" class="form-control" placeholder="Lecture" name="subject[lecture]" value="{{ old('subject.year') }}" />
					</div>
					<div class="col-md-2 form-group">
						<label class="control-label">Laboratory</label>
						<br />
						<input type="text" class="form-control" placeholder="Laboratory" name="subject[laboratory]" value="{{ old('subject.max') }}" />
					</div>
					<div class="col-md-2 form-group">
						<label class="control-label">Units</label>
						<br />
						<input type="text" class="form-control" placeholder="Units" name="subject[units]" value="{{ old('subject.max') }}" />
					</div>
				</div>				
				<div class="form-group">
					<label class="control-label">Subject Name</label>
					<br />
					<input type="text" class="form-control" placeholder="Subject Name" name="subject[name]" value="{{ old('subject.name') }}" />
				</div>
				<div class="form-group">
					<label class="control-label">Description</label>
					<br />
					<textarea class="form-control" name="subject[description]"></textarea>
				</div>
				<div class="form-group">
					<label class="control-label">Active?</label>
					<br />	                  
					<input type="radio" value="1" name="subject[is_active]" {{ ("m"==old('subject.is_active'))? 'checked' : '' }} /> Yes
					&nbsp;
					<input type="radio" value="0" name="subject[is_active]" {{ ("f"==old('subject.is_active'))? 'checked' : '' }} /> No
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary btn-flat" id="submit">Save</button>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

@stop

@section('foot')

<script type="text/javascript">
	$(function (){
		$('button#submit').click(function (){
			$.ajax({
				method: 'POST',
				url: '{{ route("subject.save") }}',
				data: $('form#add-subject').serializeArray()
			}).done(function (msg){
				console.log(msg);
				notify = msg.split('|');
			    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
			    $('.box').load("{{ route('subject.ajax.all')}}");
			});
			$('form#add-subject')[0].reset()
			$('#modal').modal('hide');
		});

		$('input[type="radio"], input[type="checkbox"]').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });

	});
</script>

@stop