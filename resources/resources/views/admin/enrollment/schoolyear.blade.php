@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="box box-primary">
			<div class="box-header">
				<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Create School Year</a>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tr>
						<th>#</th>
						<th>School Year</th>
						<th></th>
					</tr>
					@foreach($school_years as $school_year)
					<tr>
						<td>{{$school_year->id}}</td>
						<td>{{$school_year->school_year}}</td>
						<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					{!! $school_years->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('modals')

<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Add School Year</h4>
			</div>
			<form id="add-school-year">
			<div class="modal-body">
				<input class="form-control input-lg" type="text" placeholder="Enter School Year" name="school_year" id="school-year">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="submit">Save</button>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

@stop

@section('foot')

<script type="text/javascript">
	$(function (){
		$('button#submit').click(function (){
			$.ajax({
				method: 'POST',
				url: '{{ route("school_year.save") }}',
				data: $('form#add-school-year').serializeArray()
			}).done(function (msg){
				notify = msg.split('|');
			    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
			    $('.box').load("{{ route('school_year.ajax.all')}}");
			});
			$('#school-year').val('');
			$('#modal').modal('hide');
		});

	});
</script>

@stop