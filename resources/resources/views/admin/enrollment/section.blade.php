@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="box box-primary">
			<div class="box-header">
				<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Create Section</a>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Code</th>
						<th></th>
					</tr>
					@foreach($sections as $section)
					<tr>
						<td>{{$section->id}}</td>
						<td>{{$section->code}}</td>
						<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
					</tr>
					@endforeach
				</table>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					{!! $sections->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('modals')

<div class="modal fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Create Section</h4>
			</div>
			<form id="create-section">
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="input-group">
							<label class="control-label">Program</label>
							<select class="form-control" name="section[program]">
								<option value=""></option>
								@foreach ($programs as $program)
									<option value="{{ $program->id }}">{{ $program->name }}</option>
								@endforeach
							</select>
	                	</div>
	                </div>
                	<div class="col-sm-2">
						<div class="form-group">
	                      	<label class="control-label">Count</label>
	                      	<input type="email" class="form-control" name="section[count]" placeholder="Count">
	                    </div>
					</div>
					<div class="col-sm-2">
						<div class="form-group">
	                      	<label class="control-label">Code</label>
	                      	<input type="email" class="form-control" name="section[code]" placeholder="code">
	                    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="input-group">
							<label class="control-label">Year Level</label>
							<select class="form-control" id="school-year" name="section[year]">
								<option value="1">1st</option>
								<option value="2">2nd</option>
								<option value="3">3rd</option>
								<option value="4">4th</option>
							</select>
	                	</div>
	                </div>
					<div class="col-sm-4">
						<div class="input-group">
							<label class="control-label">School Year</label>
							<select class="form-control" id="school-year" name="section[sy]">
								<option value=""></option>
								@foreach ($school_years as $school_year)
									<option value="{{ $school_year->id }}">{{ $school_year->school_year }}</option>
								@endforeach
							</select>
	                	</div>
	                </div>
                	<div class="col-sm-4">
                		<div class="input-group">
		                	<label class="control-label">Semester</label>
							<select class="form-control" id="semester" name="section[semester]">
								<option value=""></option>
								@foreach ($semesters as $semester)
									<option value="{{ $semester->id }}">{{ $semester->name }}</option>
								@endforeach
							</select>
		                </div>
                	</div>
                	
				</div>
				
                
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="submit">Save</button>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

@stop

@section('foot')

<script type="text/javascript">
	$(function (){
		$('button#submit').click(function (){
			$.ajax({
				method: 'POST',
				url: '{{ route("section.save") }}',
				data: $('form#create-section').serializeArray()
			}).done(function (msg){
				notify = msg.split('|');
			    $.bootstrapGrowl(notify[1], {type: notify[0], allow_dismiss: false});
			    $('.box').load("{{ route('section.ajax.all')}}");
			});
			$('#modal').modal('hide');
		});

	});
</script>

@stop