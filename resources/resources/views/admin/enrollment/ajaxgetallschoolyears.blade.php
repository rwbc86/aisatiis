<div class="box-header">
	<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Add School Year</a>
</div>
<div class="box-body">
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>School Year</th>
			<th></th>
		</tr>
		@foreach($school_years as $school_year)
		<tr>
			<td>{{$school_year->id}}</td>
			<td>{{$school_year->school_year}}</td>
			<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
		</tr>
		@endforeach
	</table>
</div>
<div class="box-footer">
	<div class="pull-right">
		{!! $school_years->render() !!}
	</div>
</div>