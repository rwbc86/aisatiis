<div class="box-header">
	<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Create Period</a>
</div>
<div class="box-body">
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>School Year</th>
			<th>Semester</th>
			<th></th>
		</tr>
		@foreach($sections as $section)
		<tr>
			<td>{{$section->id}}</td>
			<td>{{$section->school_year}}</td>
			<td>{{$section->semester}}</td>
			<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
		</tr>
		@endforeach
	</table>
</div>
<div class="box-footer">
	<div class="pull-right">
		{!! $sections->render() !!}
	</div>
</div>