@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<h1>[{{ $subject->code }}] <small>{{ $subject->name }}</small></h1>
				<p class="text-justify">{{ $subject->description }}</p>
			</div>

			<div class="box-body">

				<div class="row">
					<div class="col-sm-3">
						<div class="btn-group">
							<a href="#" class="btn btn-default">ALL</a>
							<a href="#" class="btn btn-default">OPEN</a>
							<a href="#" class="btn btn-default">CLOSED</a>
						</div>
					</div>
					<div class="col-sm-2 col-sm-offset-7">
						<a href="{{ route('class.add', $subject->id) }}" class="btn btn-primary pull-right">ADD CLASS</a>
					</div>
				</div>

				<br />

				<div class="row">
					<div class="col-sm-12">
						<table class="table table-bordered">
							<tr>
								<th class="text-center">Class ID</th>
								<th class="text-center">School Year</th>
								<th class="text-center">Semester</th>
								<th class="text-center">Section</th>
								<th class="text-center">Slots</th>
								<th class="text-center">Instructor</th>
								<th class="text-center">Status</th>
								<th class="text-center">&nbsp;</th>
							</tr>
							@foreach ($classes as $class)
							<tr>
								<td class="text-center">{{ $class->id }}</td>
								<td class="text-center">{{ $class->school_year }}</td>
								<td class="text-center">{{ $class->name }}</td>
								<td class="text-center">{{ $class->code }}</td>
								<td class="text-center">3 / {{ $class->capacity }}</td>
								<td>{{ $class->first_name }} {{ $class->last_name }}</td>
								<td class="text-center"><h5 class="label bg-green">{{ $class->is_closed }}</h5></td>
								<td>&nbsp;</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>

			</div> <!-- /.box-body -->

			<div class="box-footer">
				<div class="pull-right">
					
				</div>
			</div>
		</div>
	</div>
</div>

@stop