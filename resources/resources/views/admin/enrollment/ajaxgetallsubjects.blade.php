<div class="box-header">
	<a href="#" id="add" class="btn btn-info btn-flat btn-small pull-right" data-toggle="modal" data-target="#modal">Add Subject</a>
</div>
<div class="box-body">
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>Code</th>
			<th>Name</th>
			<th>Lecture</th>
			<th>Laboratory</th>
			<th>Units</th>
			<th>Type</th>
			<th>&nbsp;</th>
		</tr>
		@foreach($subjects as $subject)
		<tr>
			<td>{{ $subject->id }}</td>
			<td>{{ $subject->code }}</td>
			<td>{{ $subject->name }}</td>
			<td>{{ $subject->lecture }}</td>
			<td>{{ $subject->laboratory }}</td>
			<td>{{ $subject->units }}</td>
			<td>{{ $subject->type }}</td>
			<td><a href="#" class="btn btn-danger btn-flat btn-xs pull-right"><i class="fa fa-trash"></i></a></td>
		</tr>
		@endforeach
	</table>
</div>
<div class="box-footer">
	<div class="pull-right">
		{!! $subjects->render() !!}
	</div>
</div>