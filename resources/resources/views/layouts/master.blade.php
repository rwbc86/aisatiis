<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ isset($page_name) ? $page_name.' | '  : '' }}AISAT Integrated Information System</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        
        <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/skin-black.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        @yield('head')

    </head>

    @yield('body')

    <script src="{{ asset('assets/js/jQuery-2.1.3.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery.bootstrap-growl.min.js') }}" type="text/javascript"></script>

    @yield('foot')
    
    @yield('modals')
    
  </body>
</html>


