@extends('layouts.inner_page')

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3>SELECT TRANSACTION</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-3 col-sm-offset-2">
						<div class="small-box bg-aqua">
			                <div class="inner">
			                  	<h3>Account</h3>
			                  	<p>Payment</p>
			                </div>
			                <div class="icon">
			                  	<i class="fa fa-shopping-cart"></i>
			                </div>
			                <a href="{{ route('account.select') }}" class="small-box-footer">
			                  	Start! <i class="fa fa-arrow-circle-right"></i>
			                </a>
		              	</div>
		            </div>
		            <div class="col-sm-3 col-sm-offset-1">
						<div class="small-box bg-purple">
			                <div class="inner">
			                  	<h3>Single</h3>
			                  	<p>Payment</p>
			                </div>
			                <div class="icon">
			                  	<i class="fa fa-shopping-cart"></i>
			                </div>
			                <a href="#" class="small-box-footer">
			                  	Start! <i class="fa fa-arrow-circle-right"></i>
			                </a>
		              	</div>
		            </div>
				</div>
			</div>
			<div class="box-footer">

			</div>
		</div>
	</div>
</div>

@stop