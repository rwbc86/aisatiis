@extends('layouts.inner_page')

@section('head')
@parent
<link href="{{ asset('assets/plugins/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('page_body')

<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-3 pull-left" style="margin-top: 20px;">
			<div class="input-group">
				<select class="form-control" id="students">
					<option value=""></option>
					@foreach($students as $student)
						<option value="{{ $student->id }}">{{ $student->id_number }} - {{ $student->lastname }}, {{ $student->firstname }}</option>
					@endforeach
				</select>
				<div class="input-group-btn">
                  	<button type="button" class="btn btn-primary" id="go">GO</button>
                </div>
			</div>
		</div>
		<div class="pull-right">
			{!! $students_with_pagination->render() !!}
		</div>
	</div>
</div>

<div class="row">
	@foreach($students_with_pagination as $student)
		<div class="col-md-3 student-card" onclick="window.location.href='{{ route('student.profile', array('id' => $student->id)) }}'">
	      <div class="info-box">
	        <img src="{{ (""==$student->photo)? asset('assets/img/'.$student->gender.'nophoto.jpg') : asset('photos/' . $student->photo) }}" width="90" class="info-box-icon" />
	        <div class="info-box-content">
	          <span class="info-box-text"><strong>{{ $student->lastname }}, {{ $student->firstname }}</strong></span>
	          <span class="student-card-idnum"><strong>{{ $student->id_number }}</strong></span>
	          <span class="student-card-course">2YR Aircraft Maintenance Technology</span>
	          <span class="info-box-text pull-left"><h6 class="label bg-green">Enrolled</h6></span>
	          
	          <div class="small-box-footer pull-right">
	          	<a href="{{ route('student.edit', $student->id )}}" class="btn btn-xs btn-flat btn-default"><i class="fa fa-pencil-square-o"></i></a>
			  	<a class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i></a>
	          </div>
	        </div><!-- /.info-box-content -->
	      </div><!-- /.info-box -->
	    </div>
	@endforeach
</div>



@stop

@section('foot')
@parent

<script src="{{ asset('assets/plugins/jqueryui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jqueryui/autocomplete-combo.js') }}" type="text/javascript"></script>

<script type="text/javascript">

	$(function (){
			$( "#students" ).combobox({placeholder: 'Search Student'});  
	});

	$('#go').click(function (){ 
		var url = '{{ route("student.edit", ":id") }}';
		url = url.replace(':id', $('#students').val());
		window.location.href = url ;
	});
</script>
@stop