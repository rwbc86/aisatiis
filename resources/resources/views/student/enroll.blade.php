@extends('layouts.inner_page')

@section('page_body')

<div class="box">
	<div class="box-body">
		<form action="{{ route('student.enroll.session.save') }}" method="post">
			<div class="row">
				<div class="col-sm-2">
					<img src="{{ asset('assets/img/logo2.png') }}" alt="AISAT Logo" class="logo-img" />
				</div>
				<div class="col-sm-8 text-center">
					<strong>ASIAN INTERNATIONAL SCHOOL OF AERONAUTICS AND TECHNOLOGY</strong><br />
					Sta. Ana Avenue Corner Leon Garcia St., Davao City<br />
					295-7219 / 305-7992
				</div>
				<div class="col-sm-2"></div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<h3>ENROLLMENT FORM</h3>
				</div>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2 col-sm-offset-1"><strong>Name:</strong></div>
				<div class="col-sm-2" style="border-bottom: 1px solid #000;">{{ strtoupper($student->lastname) }}</div>
				<div class="col-sm-2 col-sm-offset-1 underline">{{ strtoupper($student->firstname) }}</div>
				<div class="col-sm-2 col-sm-offset-1 underline">{{ strtoupper($student->middlename) }}</div>
			</div>

			<div class="row">
				<div class="col-sm-2 col-sm-offset-1">&nbsp;</div>
				<div class="col-sm-2 text-center"><small>SURNAME</small></div>
				<div class="col-sm-2 col-sm-offset-1 text-center"><small>FIRST NAME</small></div>
				<div class="col-sm-2 col-sm-offset-1 text-center"><small>MIDDLE NAME</small></div>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2 col-sm-offset-1"><strong>Present Address:</strong></div>
				<div class="col-sm-8 underline">{{ $student->address }}</div>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2 col-sm-offset-1"><strong>Civil Status:</strong></div>
				<div class="col-sm-3 underline">{{ $student->civil_status }}</div>
				<div class="col-sm-2 text-right"><strong>Citizenship:</strong></div>
				<div class="col-sm-3 underline">{{ $student->citizenship }}</div>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2 col-sm-offset-1"><strong>Email Address:</strong></div>
				<div class="col-sm-3 underline">{{ $student->email }}</div>
				<div class="col-sm-2 text-right"><strong>Contact Number:</strong></div>
				<div class="col-sm-3 underline">{{$student->landline}} / {{ $student->mobile }}</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-sm-5 col-sm-offset-1">
					<div class="row form-group">
						<div class="col-sm-3"><strong>School Year:</strong></div>
						<div class="col-sm-9">
							<select class="form-control" name="enroll[sy]">
								<option value=""></option>
								@foreach ($school_years as $school_year)
									<option value="{{ $school_year->id }}">{{ $school_year->school_year }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-3"><strong>Course:</strong></div>
						<div class="col-sm-9">
							<select class="form-control" name="enroll[program]">
								<option value=""></option>
								@foreach ($programs as $program)
									<option value="{{ $program->id }}">{{ $program->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-3"><strong>Section:</strong></div>
						<div class="col-sm-9">
							<select class="form-control" id="section" name="enroll[section]">
								<option value=""></option>
								@foreach ($sections as $section)
									<option value="{{ $section->id }}">{{ $section->code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-3"><strong>ID #:</strong></div>
						<div class="col-sm-9"><input type="text" class="form-control" value="{{ $student->id_number }}" disabled="disabled"/></div>
					</div>
				</div>
				<div class="col-sm-1">
					<div class="row">
						<div class="col-sm-12"><strong>Year</strong></div>
					</div>
					<br />
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[year_level]" value="1" /> 1st</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[year_level]" value="2" /> 2nd</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[year_level]" value="3" /> 3rd</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[year_level]" value="4" /> 4th</div>
					</div>	
				</div>
				<div class="col-sm-1">
					<div class="row">
						<div class="col-sm-12"><strong>Semester</strong></div>
					</div>
					<br />
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[semester]" value="1" /> 1st</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[semester]" value="2" /> 2nd</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[semester]" value="3" /> 3rd</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[semester]" value="4" /> Sum</div>
					</div>	
				</div>
				<div class="col-sm-2">
					<div class="row">
						<div class="col-sm-12"><strong>Status</strong></div>
					</div>
					<br />
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[enrollment_status]" value="1" /> New</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[enrollment_status]" value="2" /> Old</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[enrollment_status]" value="3" /> Transferee</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[enrollment_status]" value="4" /> Returnee</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[enrollment_status]" value="5" /> Shiftee</div>
					</div>	
				</div>
				<div class="col-sm-1">
					<div class="row">
						<div class="col-sm-12"><strong>Bridging</strong></div>
					</div>
					<br />
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[math_bridging]" value="1" /> Math</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12"><input type="radio" name="enroll[eng_bridging]" value="1" /> Eng</div>
					</div>	
				</div>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-5 col-sm-offset-1">
					<div class="row">
						<div class="col-sm-12">
							<strong>Classification</strong>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-sm-6">
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="1" /> Regular</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="2" /> Irregular</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="3" /> Tutorial</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="4" /> Cross Enrollee</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="5" /> Certificate Program</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="6" /> Corporate Training</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="7" /> Scholarship Grantee</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-12"><input type="radio" name="enroll[classification]" value="8" /> Others</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group" style="display:none;">
						<label class="control-label">Enter Name of Scholarhip</label>
						<br />
						<input type="text" class="form-control" placeholder="scholarhip name" name="enroll[scholarship]" value="" />
					</div>
					<div class="form-group" style="display:none;">
						<label class="control-label">Enter Custom Classification</label>
						<br />
						<input type="text" class="form-control" placeholder="scholarhip name" name="enroll[custom_classification]" value="" />
					</div>
				</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-sm-1 col-sm-offset-11">
					<div class="pull-right">
						<input type="submit" class="btn btn-flat btn-primary" value="PROCEED >>"/>
					</div>
				</div>
			</div>
			<input type="hidden" name="enroll[student]" value="{{ $student->id }}" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		</form>
	</div> <!-- END .BOX-BODY -->
</div> <!-- END .BOX -->

@stop


@section('foot')
@parent
<script src="{{ asset('assets/js/enroll.js') }}" type="text/javascript"></script>
@stop