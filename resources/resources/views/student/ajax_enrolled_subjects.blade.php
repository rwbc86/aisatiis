<table class="table table-bordered">
	<tr>
		<th colspan="8" class="text-center">CLASS INFORMATION</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th>SUBJ. CODE</th>
		<th>SUBJ. DESCRIPTION</th>
		<th>UNIT(S)</th>
		<th>DAY(S)</th>
		<th>TIME</th>
		<th>ROOM</th>
		<th>INSTRUCTOR</th>
	</tr>
	@foreach ($enrolled_classes as $class)
	<tr>
		<td><a onclick="remove_class({{ $class->id }})" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-minus"></i></a></td>
		<td>{{ $class->code }}</td>
		<td>{{ $class->name }}</td>
		<td>{{ $class->units }}</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>{{ $class->last_name }}</td>
	</tr>
	@endforeach
	<tr>
		<td colspan="3" class="text-right"><strong>TOTAL # OF UNITS: </strong></td>
		<td colspan="5">{{ $total_units }}</td>
	</tr>
</table>