<table class="table table-border">
	<tr>
		<th>Fee</th>
		<th>Amount</th>
	</tr>
	@foreach ($account_fees as $fee)
	<tr>
		<td>{{ $fee->name }}</td>
		<td>{{ $fee->amount_due }}</td>
	</tr>
	@endforeach
</table>