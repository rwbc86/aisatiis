function system_message( response ){

	data = response.split('|');

	if(data[0] == 'error'){
		icon = 'ban';
		type = 'danger';
	} else if(data[0] == 'success'){
		icon = 'check';
		type = 'success';
	}

	dialog =  '<div class="alert alert-'+ type +' alert-dismissable">';
	dialog += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
	dialog += '<i class="icon fa fa-'+icon+'"></i> '+ data[1] ;
	dialog += '</div>';

	$('#system-message').html(dialog);
}

$('input[type="radio"], input[type="checkbox"]').iCheck({
  checkboxClass: 'icheckbox_flat-green',
  radioClass: 'iradio_flat-green'
});