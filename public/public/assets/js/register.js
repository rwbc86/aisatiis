$(function (){
	
	$("[data-mask]").inputmask();

	$('input[type="radio"], input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    $('.civil-status').on('ifChecked', function (event) {
		if ($(this).val() >= 2)
    		$('.spouse-info').fadeIn();
		else	
			$('.spouse-info').fadeOut();
    });
    
    $('.is-employed').on('ifChecked', function (event) {
    	if (1 == $(this).val())
    		$('.employment-info').fadeIn();
    	else
    		$('.employment-info').fadeOut();
    });

    $('.is-true').on('ifChecked', function (event) {
   		$('#register-btn').prop('disabled',false);
    });

	$('.is-true').on('ifUnchecked', function (event) {
   		$('#register-btn').prop('disabled','disabled');
    });

    Webcam.set({
		// live preview size
		width: 200,
		height: 150,
		
		// device capture size
		dest_width: 200,
		dest_height: 150,
		
		// final cropped size
		crop_width: 150,
		crop_height: 150,
		
		image_format: 'jpeg',
		jpeg_quality: 90
	});
	
	Webcam.attach( '#my-camera' );

	$("#camera-capture").click(function (){
		Webcam.freeze();
		$("#camera-reset").show();
		$("#camera-save").show();
		$(this).hide();
	});

	$("#camera-reset").click( function() {
		Webcam.unfreeze();
		$("#camera-capture").show();
		$("#camera-save").hide();
		$(this).hide();
	});

	$("#camera-save").click( function() {
		$("#camera-reset").hide();
		$("#camera-save").hide();
		$(this).hide();
		Webcam.snap( function(data_uri) {
			var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
			$('#photo').val(raw_image_data);
			$('#photo-data').val(data_uri);
			$("#camera-area").html('<img src="'+data_uri+'" alt="Profile Photo" width="150" />');
		});
	});

	$( "#religion" ).combobox({placeholder: 'Religion'});
	$( "#citizenship" ).combobox({placeholder: 'Citizenship'});
	$( "#primary-school" ).combobox({placeholder: 'Primary School'});
	$( "#intermediate-school" ).combobox({placeholder: 'Intermediate School'});
	$( "#secondary-school" ).combobox({placeholder: 'Secondary School'});
	$( "#tertiary-school" ).combobox({placeholder: 'Tertiary School'});
	$( "#vocational-school" ).combobox({placeholder: 'Vocational School'});

});